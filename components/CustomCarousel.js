import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Dimensions, Image, Platform, ActivityIndicator, KeyboardAvoidingView } from 'react-native'
import { height, width } from '../constants/Layout';
import firebase from '../constants/firebase';
import moment from "moment";
import 'moment/locale/fr'
import { Overlay } from "react-native-elements";
import { getListsOnlineData, getListsLocalData, removeBubbleFromList, addBubbleToList, sortBubbles } from '../api/Lists';
import { addPage, editPage, removePage } from '../api/Pages';
import List from './List';
import * as SQLite from 'expo-sqlite';
import { translate } from '../locales/i18n';

const db = SQLite.openDatabase('yellowtask.db');

export default class CustomCarousel extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        bubbles: false,
        pages: false,
        editing: false,
        plusVisible: false,
        activeIndex: 0,
        newBubble: undefined,
        newPage: undefined,
        pageDelete: undefined,
        repeated: [],
        checked: [],
        notUsed: [],
        carouselItems: [],
        alreadyLoaded: false,
        infoLoaded: false,
    };

    findDate(items, date) {
        for (let i in items)
            if (items[i].date === date)
                return i;
        return -1;
    }

    isRepeated(repeat) {
        for (let i in repeat) {
            if (repeat[i] === true)
                return true;
        }
        return false;
    }

    isDayInRepeated(day, repeated) {
        for (let i in repeated) {
            if (repeated[i].repeat[day.toLowerCase()] === true)
                return true;
        }
        return false;
    }

    isAlreadyInList(bubble, page) {
        for (let i in page.bubbles) {
            if (page.bubbles[i] === bubble)
                return true;
        }
        return false;
    }

    clearData() {
        this.setState({ bubbles: false, pages: false, editing: false, plusVisible: false, activeIndex: 0, newBubble: undefined, newPage: undefined, pageDelete: undefined, repeated: [], checked: [], notUsed: [], carouselItems: [], alreadyLoaded: false });
    }

    checkRepeat(carouselItems) {
        let repeated = [];
        let day;
        let currentDate = moment().format('L');

        for (let i in carouselItems) {
            for (let j in carouselItems[i].bubbles) {
                if (carouselItems[i].bubbles[j].repeat && this.isRepeated(carouselItems[i].bubbles[j].repeat))
                    repeated.push(carouselItems[i].bubbles[j]);
            }
        }
        for (let i in carouselItems) {
            for (let j in repeated) {
                day = moment(new Date(carouselItems[i].date[3] + carouselItems[i].date[4] + "/" + carouselItems[i].date.slice(0, 3) + carouselItems[i].date.slice(6))).locale('en').format('dddd');
                if (this.isDayInRepeated(day, repeated) && carouselItems[i].date !== currentDate && !this.isAlreadyInList(repeated[j], carouselItems[i]))
                    carouselItems[i].bubbles.push(repeated[j]);
            }
        }
        return carouselItems;
    }

    newSortBubblesPerso(pages) {
        let newItems = [];
        for (let i in pages) {
            newItems.push({
                page: pages[i].name,
                bubbles: pages[i].bubbles,
            });
        }
        newItems.push({ page: "", bubbles: [] });
        return newItems;
    }

    sortNewBubbles() {
        let { carouselItems, notUsed } = this.state;
        let initialLength = carouselItems.length;
        let currentDate = moment().locale('en').format('L');
        let currentDateFR = moment().locale('fr').format('L');
        let newNotUsed = [];
        let compareDate = -1;

        for (let dayIndex = carouselItems.length; dayIndex !== initialLength + 28; dayIndex++) {
            let date = moment().add(dayIndex, 'days').format('L');
            carouselItems.push({ date, bubbles: [] });
        }
        for (let i in notUsed) {
            if (notUsed[i].date && moment(new Date(currentDate).toISOString()).isAfter(moment(new Date(notUsed[i].date[3] + notUsed[i].date[4] + "/" + notUsed[i].date.slice(0, 3) + notUsed[i].date.slice(6)).toISOString()).toISOString())) {
                compareDate = this.findDate(carouselItems, currentDateFR);
                if (compareDate === -1)
                    carouselItems.push({ date: currentDateFR, bubbles: [notUsed[i]] });
                else
                    carouselItems[compareDate].bubbles.push(notUsed[i]);
            } else {
                compareDate = this.findDate(carouselItems, notUsed[i].date ? notUsed[i].date : currentDateFR);
                if (compareDate !== -1)
                    carouselItems[compareDate].bubbles.push(notUsed[i]);
                else
                    newNotUsed.push(notUsed[i]);
            }
        }
        carouselItems = this.checkRepeat(carouselItems);
        this.setState({ carouselItems, notUsed: newNotUsed });
    }

    async componentDidUpdate(prevProps) {
        if (prevProps === this.props)
            return;
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        if (!this.props.lists[this.props.selected])
            return;
        let res;
        this.setState({ infoLoaded: false });
        if (this.props.newSession) {
            res = await getListsOnlineData(db, this.props.lists, this.props.selected, this.state);
            this.props.updateSession();
        } else {
            if (this.props.localData || this.state.alreadyLoaded)
                res = await getListsLocalData(db, this.props.time, true, this.props.lists[this.props.selected], this.props.lists, user, this.state);
            else
                res = await getListsOnlineData(db, this.props.time, this.props.lists, this.props.selected, this.state);
        }
        let type = this.props.lists[this.props.selected] ? this.props.lists[this.props.selected].type : "UNDEFINED";
        let carouselItems = res.carouselItems;
        // if (type === 'calendar')
        //     carouselItems = this.getRepeatedAll(res.carouselItems);
        this.setState({ carouselItems: carouselItems, bubbles: res.bubbles, pages: res.pages, notUsed: res.notUsed, alreadyLoaded: true, infoLoaded: true });
    }

    getRepeatedAll(carouselItems) {
        let day;
        let repeated = [];

        for (let i in carouselItems) {
            for (let j in carouselItems[i].bubbles) {
                if (carouselItems[i].bubbles[j].repeat && this.isRepeated(carouselItems[i].bubbles[j].repeat))
                    repeated.push(carouselItems[i].bubbles[j]);
            }
        }
        for (let i in carouselItems) {
            carouselItems[i].repeated = [];
            for (let j in carouselItems[i].bubbles) {
                if (!carouselItems[i].date[3])
                    return;
                day = moment(new Date(carouselItems[i].date[3] + carouselItems[i].date[4] + "/" + carouselItems[i].date.slice(0, 3) + carouselItems[i].date.slice(6))).locale('en').format('dddd');
                if (carouselItems[i].bubbles[j].repeat && carouselItems[i].bubbles[j].repeat !== undefined && this.hasTrue(carouselItems[i].bubbles[j].repeat) && this.isDayInRepeatedReal(day, carouselItems[i].bubbles[j])) {
                    carouselItems[i].repeated.push({ bubble: carouselItems[i].bubbles[j], date: carouselItems[i].date });
                }
            }
        }
        return carouselItems;
    }

    removeBubble = async (id, time) => {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        let res = await removeBubbleFromList(time, this.state, this.props, id, db);
        if (res.carouselItems)
            this.setState({ carouselItems: res.carouselItems });
        if (res.bubbles)
            this.setState({ bubbles: res.bubbles });
        else if (res.pages)
            this.setState({ pages: res.pages });
        this.props.analytics.track("Del Past", { "None": "None" });
    };

    addBubble = async (data, time, date) => {
        const user = firebase.auth().currentUser;
        let type = this.props.lists ? this.props.lists[this.props.selected].type : null;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        let res = await addBubbleToList(time, this.state, this.props, type, db, user, data, date);
        if (res.carouselItems)
            this.setState({ carouselItems: res.carouselItems });
        if (res.bubbles)
            this.setState({ bubbles: res.bubbles });
        else if (res.pages)
            this.setState({ pages: res.pages });
        this.props.analytics.track("Add Past", { "None": "None" });
    };

    endReached = () => {
        //this.setState({ activeIndex: viewableItems[0].index });
        let type = this.props.lists ? this.props.lists[this.props.selected].type : "";

        if (type === 'calendar') {
            let { bubbles, notUsed, activeIndex } = this.state;
            //if (!checked.includes(viewableItems[0].index)) {
            //    checked.push(viewableItems[0].index);
            //    this.setState({ checked });
            let carouselItems = sortBubbles(bubbles, notUsed, activeIndex, this.props.time);
            this.setState({ carouselItems })
            //}
        }
    };

    onViewableItemsChanged = ({ viewableItems }) => {
        if (viewableItems.length > 0) {
            this.setState({ activeIndex: viewableItems[0].index });
            this.props.analytics.track("Change-day", { "None": "None" });
        }
    }

    async editPage(item, index) {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        if (!this.state.newPage)
            return;
        if (this.state.pages.length === index) {
            // ADDING PAGE
            let res = await addPage(this.state, this.props, user, db);
            this.setState({ pages: res.pages, carouselItems: res.carouselItems, newPage: undefined });
            this.props.analytics.track("Add-cate", { "None": "None" });
        } else {
            // CHANGING PAGE INFOS
            let res = editPage(this.state, index, db);
            this.setState({ carouselItems: res.carouselItems, newPage: undefined });
        }
    }

    async removePage(time, page) {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        if (page === null)
            return;
        let res = removePage(time, this.state, this.props, page, db);
        this.setState({ carouselItems: res.carouselItems, toDelete: res.toDelete, pageDelete: res.pageDelete, plusVisible: res.plusVisible, pages: res.pages });
    }

    hasTrue(repeat) {
        for (let i in repeat) {
            if (repeat[i] === true)
                return true;
        }
        return false;
    }

    isDayInRepeatedReal(day, repeated) {
        if (repeated.repeat[day.toLowerCase()] === true)
            return true;
        return false;
    }

    // getRepeated() {
    //     let { carouselItems } = this.state;
    //     let day;
    //     let repeated = [];

    //     for (let i in carouselItems) {
    //         for (let j in carouselItems[i].bubbles) {
    //             if (carouselItems[i].bubbles[j].repeat && this.isRepeated(carouselItems[i].bubbles[j].repeat))
    //                 repeated.push(carouselItems[i].bubbles[j]);
    //         }
    //     }

    //     for (let i in carouselItems) {
    //         carouselItems[i].repeated = [];
    //         for (let j in carouselItems[i].bubbles) {
    //             day = moment(new Date(carouselItems[i].date[3] + carouselItems[i].date[4] + "/" + carouselItems[i].date.slice(0, 3) + carouselItems[i].date.slice(6))).locale('en').format('dddd');
    //             if (carouselItems[i].bubbles[j].repeat && carouselItems[i].bubbles[j].repeat !== undefined && this.isDayInRepeatedReal(day, carouselItems[i].bubbles[j]) && this.hasTrue(carouselItems[i].bubbles[j].repeat)) {
    //                 carouselItems[i].repeated.push({ bubble: carouselItems[i].bubbles[j], date: carouselItems[i].date });
    //             }
    //         }
    //     }

    //     this.setState({ carouselItems });
    // }

    render() {
        if (this.props.selected !== -1 && this.state.infoLoaded) {
            let type = this.props.lists[this.props.selected] ? this.props.lists[this.props.selected].type : "UNDEFINED";
            return (
                <>
                    {this.state.carouselItems ?
                        <FlatList
                            style={{ height: this.props.layout }}
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.carouselItems}
                            onViewableItemsChanged={this.onViewableItemsChanged}
                            onEndReached={this.endReached}
                            onEndReachedThreshold={50}
                            viewabilityConfig={{ itemVisiblePercentThreshold: 5 }}
                            extraData={this.state}
                            renderItem={({ item, index }) => <List increaseTagIndex={this.props.increaseTagIndex} addTag={(newTagName, newTagColor) => this.props.addTag(newTagName, newTagColor)} removeTag={(index) => this.props.removeTag(index)} editTag={(newTagName, newTagColor, newId, index) => this.props.editTag(newTagName, newTagColor, newId, index)} time={this.props.time} tags={this.props.tags} activeIndex={this.state.activeIndex} carouselItems={this.state.carouselItems} item={item} index={index} type={type} addBubble={(data, date) => this.addBubble(data, this.props.time, date)} removeBubble={(data) => this.removeBubble(data, this.props.time)} updateBubble={this.endReached} editPage={() => this.editPage(item, index)} editPageName={(e) => this.setState({ newPage: e })} openPageDel={() => this.setState({ plusVisible: true, pageDelete: index })} confettiRef={this.props.confettiRef} analytics={this.props.analytics} />}
                            pagingEnabled={true}
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            decelerationRate={0}
                            snapToInterval={Dimensions.get("window").width - (width * 0.12)}
                            snapToAlignment={"left"}
                            centerContent={false}
                            contentInset={{
                                top: 0,
                                left: 0,
                                bottom: 0,
                                right: width * 0.12
                            }}
                        />
                        : <View />}
                    <Overlay isVisible={this.state.plusVisible} onBackdropPress={() => this.setState({ plusVisible: false })}
                        overlayBackgroundColor={'rgba(0, 0, 0, 0)'} windowBackgroundColor={'rgba(0, 0, 0, 0)'}
                        overlayStyle={{ width: (width * 0.8), height: 50, backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                        <View style={{
                            position: 'absolute',
                            right: width * 0.02,
                            bottom: height * 0.2,
                            borderRadius: 15,
                            backgroundColor: '#fff',
                            height: 50,
                            width: (width * 0.8),
                            alignItems: 'center',
                            flexDirection: 'row',
                            padding: 10
                        }}>
                            <TouchableOpacity activeOpacity={100} onPress={() => this.removePage(this.props.time, this.state.pageDelete)}
                                style={{ alignItems: 'center', flexDirection: 'row' }}>
                                <Image source={require('../assets/blackbin.png')}
                                    resizeMode={"contain"}
                                    style={{
                                        height: 20,
                                        width: 20,
                                        marginBottom: 7
                                    }} />
                                <Text numberOfLines={1} style={{
                                    paddingHorizontal: 10,
                                    textAlign: 'left',
                                    fontSize: 18,
                                    color: '#000',
                                    fontFamily: 'avenirbook'
                                }} adjustsFontSizeToFit>{translate('delete')}</Text>
                            </TouchableOpacity>
                        </View>
                    </Overlay>
                </>
            );
        } else {
            return (
                <View style={{ height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }} >
                    <ActivityIndicator size="large" />
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 15,
        elevation: 15
    }
});