import * as React from "react"
import Svg, { Circle } from "react-native-svg"

function UnderNotArchived(props) {
  return (
    <Svg
      width={18}
      height={18}
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Circle cx={8.75} cy={8.75} r={8.25} stroke={props.color} strokeOpacity={0.7} />
    </Svg>
  )
}

export default UnderNotArchived;
