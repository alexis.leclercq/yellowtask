import * as React from "react"
import Svg, { Ellipse } from "react-native-svg"

function ThreeDots(props) {
  return (
    <Svg
      width={21}
      height={6}
      viewBox="0 0 21 6"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Ellipse
        cx={2.607}
        cy={2.985}
        rx={2.297}
        ry={2.295}
        transform="rotate(-90 2.607 2.985)"
        fill={props.color}
        fillOpacity={0.15}
      />
      <Ellipse
        cx={10.297}
        cy={2.985}
        rx={2.297}
        ry={2.295}
        transform="rotate(-90 10.297 2.985)"
        fill={props.color}
        fillOpacity={0.15}
      />
      <Ellipse
        cx={17.988}
        cy={2.985}
        rx={2.297}
        ry={2.295}
        transform="rotate(-90 17.988 2.985)"
        fill={props.color}
        fillOpacity={0.15}
      />
    </Svg>
  )
}

export default ThreeDots
