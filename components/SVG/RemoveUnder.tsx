import * as React from "react"
import Svg, { Circle, Path } from "react-native-svg"

function RemoveUnder(props) {
  return (
    <Svg
      width={16}
      height={16}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Circle cx={8} cy={8} r={8} fill="#DFDBD6" />
      <Path
        d="M5 5l6 6M11 5l-6 6"
        stroke={props.color}
        strokeOpacity={0.9}
        strokeWidth={0.5}
      />
    </Svg>
  )
}

export default RemoveUnder;
