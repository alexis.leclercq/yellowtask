import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Repeat(props) {
  return (
    <Svg
      width={19}
      height={16}
      viewBox="0 0 19 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M2.72 8.26V8a8 8 0 111.208 4.23.445.445 0 01.754-.47c.388.62.87 1.178 1.426 1.653A7.111 7.111 0 103.608 8v.26l1.02-1.019a.444.444 0 11.628.629L3.478 9.648a.444.444 0 01-.628 0L1.072 7.87a.444.444 0 11.629-.629l1.019 1.02zm11.286.665a.444.444 0 01-.35.817l-3.111-1.333a.445.445 0 01-.247-.268l-1.333-4a.444.444 0 01.843-.282l1.27 3.811 2.928 1.255z"
        fill={props.color}
        fillOpacity={0.4}
      />
    </Svg>
  )
}

export default Repeat
