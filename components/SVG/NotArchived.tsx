import * as React from "react"
import Svg, { Circle } from "react-native-svg"

function NotArchived(props) {
  return (
    <Svg
      width={28}
      height={28}
      viewBox="0 0 28 28"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Circle cx={14} cy={14} r={13.5} stroke={props.color} strokeOpacity={0.7} />
    </Svg>
  )
}

export default NotArchived
