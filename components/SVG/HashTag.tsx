import * as React from "react"
import Svg, { Path } from "react-native-svg"

function HashTag(props) {
  return (
    <Svg
      width={9}
      height={13}
      viewBox="0 0 9 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M3.06 7.378h2.261l.748-2.754H3.808L3.06 7.378zm-1.326 1.02H0l.153-1.02h1.87l.748-2.754H.935l.153-1.02h1.989L4.08 0h1.037L4.114 3.604h2.261L7.378 0h1.037L7.412 3.604h1.53l-.153 1.02H7.106l-.748 2.754h1.615l-.153 1.02H6.069l-1.02 3.638H4.012l1.02-3.638H2.771l-1.02 3.638H.714l1.02-3.638z"
        fill={props.color}
        fillOpacity={0.4}
      />
    </Svg>
  )
}

export default HashTag
