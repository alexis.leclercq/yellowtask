import * as React from "react"
import Svg, { Path } from "react-native-svg"

function Reminder(props) {
  return (
    <Svg
      width={16}
      height={16}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M8 16A8 8 0 118 0a8 8 0 010 16zm0-.8A7.2 7.2 0 108 .8a7.2 7.2 0 000 14.4zm3.033-5.525a.4.4 0 11-.465.65l-2.8-2a.4.4 0 01-.154-.22l-1.2-4.4a.4.4 0 01.772-.21l1.162 4.262 2.684 1.918z"
        fill={props.color}
        fillOpacity={0.4}
      />
    </Svg>
  )
}

export default Reminder
