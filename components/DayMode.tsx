import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { TextInverted as ThemedTextInverted } from './common/Themed';
import { translate } from '../locales/i18n';

interface DayModeProps {
    lists: Array<any>;
    analytics: any;
    selected: number;
    time: string;
    setTime: any;
}

export default function DayMode(props: DayModeProps) {
    if (props.lists && props.lists.length > 0 && props.lists[props.selected].type && props.lists[props.selected].type == "calendar")
        return (
            <>
                <TouchableOpacity onPress={() => { props.setTime('day'); props.analytics.track("Day Click", { "None": "None" }); }} style={{ alignItems: 'center' }}>
                    <ThemedTextInverted style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontFamily: 'avenirbook',
                    }} adjustsFontSizeToFit>{translate("day")}</ThemedTextInverted>
                    {props.time === 'day' && <ThemedTextInverted style={{ textAlign: 'center', fontSize: 17, fontFamily: 'avenirbook', marginTop: -3 }} adjustsFontSizeToFit>•</ThemedTextInverted>}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { props.setTime('week'); props.analytics.track("Week Click", { "None": "None" }); }} style={{ alignItems: 'center' }}>
                    <ThemedTextInverted style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontFamily: 'avenirbook',
                        marginLeft: 20,
                    }} adjustsFontSizeToFit>{translate("week")}</ThemedTextInverted>
                    {props.time === 'week' && <ThemedTextInverted style={{ textAlign: 'center', fontSize: 17, fontFamily: 'avenirbook', marginTop: -3, marginLeft: 7 }} adjustsFontSizeToFit>•</ThemedTextInverted>}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { props.setTime('month'); props.analytics.track("Month Click", { "None": "None" }); }} style={{ alignItems: 'center' }}>
                    <ThemedTextInverted style={{
                        textAlign: 'center',
                        fontSize: 20,
                        fontFamily: 'avenirbook',
                        marginLeft: 20,
                    }} adjustsFontSizeToFit>{translate("month")}</ThemedTextInverted>
                    {props.time === 'month' && <ThemedTextInverted style={{ textAlign: 'center', fontSize: 17, fontFamily: 'avenirbook', marginTop: -3, marginLeft: 14 }} adjustsFontSizeToFit>•</ThemedTextInverted>}
                </TouchableOpacity>
            </>
        )
    return <View />;
}