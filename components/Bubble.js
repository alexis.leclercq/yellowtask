import React, { Component } from 'react';
import { height, width } from "../constants/Layout";
import { Image, StyleSheet, Platform, FlatList, AsyncStorage, LayoutAnimation, YellowBox, Button, ScrollView, View, TouchableOpacity, TextInput, Text } from "react-native";
import { View as ThemedView, TouchableOpacity as ThemedTouchableOpacity, TextInput as ThemedTextInput, Text as ThemedText } from '../components/common/Themed';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Overlay } from "react-native-elements";
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment";
import CheckBox from "react-native-check-box";
import SwipeablePanel from "rn-swipeable-panel";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as ExpoHaptics from 'expo-haptics';
import { BlurView } from 'expo-blur';
import { translate } from '../locales/i18n';
import ThreeDots from './SVG/ThreeDots';
import HashTag from './SVG/HashTag';
import Reminder from './SVG/Reminder';
import Archived from './SVG/Archived';
import NotArchived from './SVG/NotArchived';
import UnderArchived from './SVG/UnderArchived';
import UnderNotArchived from './SVG/UnderNotArchived';
import RemoveUnder from './SVG/RemoveUnder';

YellowBox.ignoreWarnings([
    'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

async function getiOSNotificationPermission() {
    const { status } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    if (status !== 'granted') {
        await Permissions.askAsync(Permissions.NOTIFICATIONS);
    }
}

export default class Bubble extends Component {

    constructor(props) {
        super(props);

        this._isMounted = false;
    }

    state = {
        item: null,
        editSelected: 'none',
        date: new Date(),
        error: false,
        textEditing: false,
        swipeablePanelActive: false,
        startingIndex: 0,
        showMore: false,
        showMoreAdd: false,
        showMoreDrop: false,
        showMoreTag: false,
        showMoreReminder: false,
        dateActivate: false,
        addTag: false,
        newTag: null,
        removeTag: false,
        tagIndex: null,
        openTag: null,
        newColor: null,
        newId: null,
        openTagPosition: {
            width: null,
            height: null,
            x: null,
            y: null,
        },
    };

    async componentDidMount() {
        this._isMounted = true;
        if (this.props.item) {
            if (await AsyncStorage.getItem('YellowTaskShowMoreTag'))
                this._isMounted && this.setState({ showMoreTag: true });
            if (await AsyncStorage.getItem('YellowTaskShowMoreReminder'))
                this._isMounted && this.setState({ showMoreReminder: true });
            this.getDays(this.props.item);
            if (this.props.item.open && this.props.item.open !== "false") {
                if (this.props.item.note || (this.props.item.tag && this.props.item.tag.length > 0) || (this.props.item.underTask && this.props.item.underTask.length > 0))
                    this._isMounted && this.setState({ showMore: true });
                else
                    this._isMounted && this.setState({ showMoreAdd: true });
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    listenForNotifications = () => {
        Notifications.addListener(notification => {
            if (notification.origin === 'received' && Platform.OS === 'ios') {
                //Alert.alert(notification.origin);
            }
        });
    };

    async sendNotif() {
        const localnotification = {
            title: 'YellowTask',
            body: this.state.item.name,
            android: {
                sound: true,
            },
            ios: {
                sound: true,
            },
        };
        let date = new Date(this.state.date);

        await getiOSNotificationPermission();
        this.listenForNotifications();
        const schedulingOptions = { time: date };
        let id = Notifications.scheduleLocalNotificationAsync(
            localnotification,
            schedulingOptions
        );
    };

    async componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.item !== this.props.item) {
            if (await AsyncStorage.getItem('YellowTaskShowMoreTag'))
                this._isMounted && this.setState({ showMoreTag: true });
            if (await AsyncStorage.getItem('YellowTaskShowMoreReminder'))
                this._isMounted && this.setState({ showMoreReminder: true });
            this.getDays(this.props.item);
            if (this.props.item.open && this.props.item.open !== "false") {
                if (this.props.item.note || (this.props.item.tag && this.props.item.tag.length > 0) || (this.props.item.underTask && this.props.item.underTask.length > 0))
                    this._isMounted && this.setState({ showMore: true });
                else
                    this._isMounted && this.setState({ showMoreAdd: true });
            }
        }
    }

    editText(e) {
        let { item } = this.state;
        item.name = e;
        this._isMounted && this.setState({ item });
    }

    getArchivedIndex(item) {
        for (let i in item.archived) {
            if (item.archived[i] === this.props.date)
                return i;
        }
        return -1;
    }

    archive() {
        let { item } = this.state;
        if (this.state.textEditing)
            return;
        ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Medium)
        if (this.hasTrue()) {
            if (item.archived.includes(this.props.date)) {
                let index = this.getArchivedIndex(item);
                if (this.props.date !== moment(new Date()).format('L')) {
                    this._isMounted && this.setState({ error: true });
                    return;
                }
                if (!item.archived || !item.archived.length)
                    item.archived = [];
                if (index !== -1) {
                    item.archived.splice(index, 1);
                    if (item.days - 1 >= 0)
                        item.days--;
                }
            } else {
                if (this.props.confettiRef)
                    this.props.confettiRef.startConfetti();
                this.props.analytics.track("Archiv Hab", { "None": "None" });
                let index = this.getArchivedIndex(item);

                if (this.props.date !== moment(new Date()).format('L')) {
                    this._isMounted && this.setState({ error: true });
                    return;
                }
                if (!item.archived || !item.archived.length)
                    item.archived = [];
                if (index === -1) {
                    item.archived.push(this.props.date);
                    item.days++;
                }
            }
        } else {
            if (item.archived) {
                item.archived = false;
                this.props.analytics.track("Back-Semi-A", { "None": "None" });
                LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                this.props.update();
            } else {
                if (this.props.confettiRef)
                    this.props.confettiRef.startConfetti();
                this.props.analytics.track("Semi Archiv", { "None": "None" });
                item.archived = true;
                LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                this.props.update();
            }
        }
        this._isMounted && this.setState({ item });
        this.endEditing().then(r => null);
    }

    delete() {
        if (!this.hasTrue()) {
            ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy)
            if (this.props.confettiRef)
                this.props.confettiRef.startConfetti();
            this.props.deleteBubble(this.state.item.ref);
        }
    }

    editColor(e) {
        let { item } = this.state;
        item.color = e;
        this.props.analytics.track("Edit Color", { "None": "None" });
        this._isMounted && this.setState({ item });
        this.endEditing().then(r => null);
    }

    async endEditing() {
        let db = this.props.dataBase;
        if (!this.state.item.page) {
            db.transaction(
                tx => {
                    tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                    tx.executeSql("select * from bubbles where id = (?)", [this.state.item.ref], async (_, { rows }) => {
                        if (rows.length != 0) {
                            tx.executeSql("update bubbles set name = (?), date = (?), archived = (?), days = (?), reminders = (?), repeat = (?), note = (?), tag = (?), underTask = (?), open = (?) where id = (?)", [
                                this.state.item.name,
                                this.state.item.date ? this.state.item.date : moment(new Date()).format('L'),
                                this.state.item.archived ? JSON.stringify(this.state.item.archived) : false,
                                this.state.item.days ? this.state.item.days : 0,
                                this.state.item.reminders ? JSON.stringify(this.state.item.reminders) : null,
                                this.state.item.repeat ? JSON.stringify(this.state.item.repeat) : null,
                                this.state.item.note ? this.state.item.note : null,
                                this.state.item.tag ? JSON.stringify(this.state.item.tag) : null,
                                this.state.item.underTask ? JSON.stringify(this.state.item.underTask) : null,
                                this.state.item.open ? this.state.item.open : "false",
                                this.state.item.ref
                            ]);
                            AsyncStorage.setItem("yellowtaskDBNeedUpdate" + this.state.item.ref, "true");
                        }
                    });
                }
            );
        } else {
            db.transaction(
                tx => {
                    tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                    tx.executeSql("select * from bubbles where id = (?)", [this.state.item.ref], async (_, { rows }) => {
                        if (rows.length != 0) {
                            tx.executeSql("update bubbles set name = (?), page = (?), archived = (?), reminders = (?), repeat = (?), note = (?), tag = (?), underTask = (?), open = (?) where id = (?)", [
                                this.state.item.name,
                                this.state.item.page,
                                this.state.item.archived ? JSON.stringify(this.state.item.archived) : false,
                                this.state.item.reminders ? JSON.stringify(this.state.item.reminders) : null,
                                this.state.item.repeat ? JSON.stringify(this.state.item.repeat) : null,
                                this.state.item.note ? this.state.item.note : null,
                                this.state.item.tag ? JSON.stringify(this.state.item.tag) : null,
                                this.state.item.underTask ? JSON.stringify(this.state.item.underTask) : null,
                                this.state.item.open ? this.state.item.open : "false",
                                this.state.item.ref
                            ]);
                            AsyncStorage.setItem("yellowtaskDBNeedUpdate" + this.state.item.ref, "true");
                        }
                    });
                }
            );
        }
    }

    addReminder() {
        let { item } = this.state;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (!item.reminders)
            item.reminders = [];
        this.props.analytics.track("Ajout Rappel", { "None": "None" });
        item.reminders.push(this.state.date.toUTCString());
        this._isMounted && this.setState({ item });
        this.sendNotif();
        this.endEditing().then(r => null);
    }

    removeReminder(key) {
        let { item } = this.state;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        item.reminders.splice(key, 1);
        this._isMounted && this.setState({ item });
        this.endEditing().then(r => null);
    }

    getLastDay(item) {
        if (this.hasTrueItem(item)) {
            if (!item.archived || !item.archived.length)
                item.archived = [];
            for (let i = 0; i != 7; i++) {
                item.archived.push(moment(new Date()).subtract(i, 'days').format('L'));
            }
        }
        return item;
    }

    addRepeat(key, state) {
        let { item } = this.state;
        let check = false;

        if (!item.repeat)
            item.repeat = {
                monday: false,
                tuesday: false,
                wednesday: false,
                thursday: false,
                friday: false,
                saturday: false,
                sunday: false
            };
        else {
            check = true;
        }
        item.repeat[key] = state;
        if (check === true)
            item = this.getLastDay(item);
        this.props.analytics.track("Ajout Hab", { "None": "None" });
        this._isMounted && this.setState({ item });
        this.endEditing().then(r => null);
    }

    hasTrue() {
        for (let i in this.state.item.repeat) {
            if (this.state.item.repeat[i] === true)
                return true;
        }
        return false;
    }

    hasTrueItem(item) {
        for (let i in item.repeat) {
            if (item.repeat[i] === true)
                return true;
        }
        return false;
    }

    getDays(item) {
        if (this.hasTrueItem(item)) {
            if (!item.archived || !item.archived.length)
                item.archived = [];
            let i = 1;
            for (; !this.isRepeated(item, moment(new Date()).locale('en').subtract(i, 'days').format('dddd').toLowerCase()); i++);
            if (!item.archived.includes(moment(new Date()).locale('fr').subtract(i, 'days').format('L'))) {
                item.days = 0;
            }
        }
        this._isMounted && this.setState({ item });
    }

    isRepeated(item, date) {
        if (item.repeat[date] === true)
            return true;
        return false;
    }

    openPanel = () => {
        this._isMounted && this.setState({ swipeablePanelActive: true });
    };

    closePanel = () => {
        this._isMounted && this.setState({ swipeablePanelActive: false });
    };

    addNewUnderTask(e) {
        const { underTask } = this.state.item;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.props.analytics.track("add Sous-t", { "None": "None" });
        if (e.nativeEvent.text.length == 0)
            return;
        underTask.push({ id: underTask.length, name: e.nativeEvent.text, archived: false });
        this._isMounted && this.setState({ item: { ...this.state.item, underTask } });
    }

    editUnderTask(e, id) {
        const { underTask } = this.state.item;

        if (e.nativeEvent.text.length == 0)
            return;
        underTask[id] = { ...underTask[id], name: e.nativeEvent.text };
        this._isMounted && this.setState({ item: { ...this.state.item, underTask } });
    }

    removeUnderTask(id) {
        const { underTask } = this.state.item;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        underTask.splice(id, 1);
        this._isMounted && this.setState({ item: { ...this.state.item, underTask } });
        this.endEditing();
    }

    addTag(index) {
        const { tag } = this.state.item;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy)
        this.props.analytics.track("add id", { "None": "None" });
        let check;
        let newTag = tag;
        if (newTag == null) {
            newTag = [];
            check = -1;
        } else
            check = newTag.indexOf(index);
        if (check !== -1)
            newTag.splice(check, 1);
        else
            newTag.push(index);
        this._isMounted && this.setState({ item: { ...this.state.item, tag: newTag } });
    }

    changeLayout = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this.state.item.open && this.state.item.open === "true")
            this.props.analytics.track("Close-p", { "None": "None" });
        else
            this.props.analytics.track("Open-p", { "None": "None" });
        this._isMounted && this.setState({ showMoreDrop: false, showMore: !this.state.showMore, item: { ...this.state.item, open: this.state.item.open && this.state.item.open === "true" ? "false" : "true" } });
        this.endEditing();
    }

    changeLayoutAdd = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this.state.item.open && this.state.item.open === "true")
            this.props.analytics.track("Close-p", { "None": "None" });
        else
            this.props.analytics.track("Open-p", { "None": "None" });
        this._isMounted && this.setState({ showMoreDrop: false, showMoreAdd: !this.state.showMoreAdd, item: { ...this.state.item, open: this.state.item.open && this.state.item.open === "true" ? "false" : "true" } });
        this.endEditing();
    }

    changeLayoutDrop = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (!this.state.showMoreDrop)
            this.props.analytics.track("Open me-p", { "None": "None" });
        this._isMounted && this.setState({ showMoreDrop: !this.state.showMoreDrop, showMore: false, showMoreAdd: false, item: { ...this.state.item } });
        this.endEditing();
    }

    hasMultiTags() {
        for (let i in this.props.tags) {
            if (hasTag(this.props.tags[i].id, this.state.item.tag))
                return true;
        }
        return false;
    }

    getTagInfoById(id) {
        for (let i in this.props.tags) {
            if (this.props.tags[i].id === id)
                return this.props.tags[i];
        }
        return null;
    }

    render() {
        if (this.state.item) {
            const { repeat, tag, underTask, name, note } = this.state.item;
            return (
                <View>
                    <ThemedView style={{
                        width: width * 0.8,
                        marginLeft: 30,
                        borderRadius: 26,
                        minHeight: 50,
                        marginBottom: 13,
                    }}>
                            {tag && tag.length > 0 && this.hasMultiTags() && (
                                <View style={{
                                    paddingTop: 0,
                                }}>
                                    <View style={{ paddingTop: 15, paddingBottom: 0, paddingHorizontal: 10, flexDirection: 'row', flexWrap: "wrap" }}>
                                        {tag.map((data, index) => {
                                            let tagData = this.getTagInfoById(data);
                                            if (tagData) {
                                                return (
                                                    <Tag key={index} color={tagData.color} text={tagData.name.toUpperCase()} />
                                                );
                                            }
                                            return null;
                                        })}
                                    </View>
                                </View>
                            )}
                            <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => null} style={{
                                flexDirection: 'row',
                                paddingLeft: 20,
                            }}>
                                <TouchableOpacity activeOpacity={100} onLongPress={this.props.onLongPress} onPressOut={this.props.onPressOut} onPress={() => {
                                    if ((this.state.item && this.state.item.note) || (underTask && underTask.length > 0))
                                        this.changeLayout();
                                    else
                                        this.changeLayoutAdd();
                                }}>
                                    {(this.state.showMoreAdd || this.state.showMoreDrop) ? (
                                        <ThemedTextInput keyboardType={Platform.OS === "ios" ? "web-search" : "default"} onEndEditing={() => this.endEditing()} onChangeText={(e) => this._isMounted && this.setState({ item: { ...this.state.item, name: e } })} style={{
                                            alignSelf: 'center',
                                            width: (width * 0.8) - 72,
                                            textAlign: 'left',
                                            textDecorationLine: this.hasTrue() ? this.getArchivedIndex(this.state.item) === -1 ? 'none' : 'line-through' : this.state.item.archived ? 'line-through' : 'none',
                                            fontSize: 17,
                                            fontFamily: 'avenirroman',
                                            paddingTop: 3,
                                            paddingRight: 10,
                                            marginTop: 14,
                                            marginBottom: 14
                                        }}>{this.state.item.name}</ThemedTextInput>
                                    ) : (
                                            <ThemedText style={{
                                                alignSelf: 'center',
                                                width: (width * 0.8) - 72,
                                                textAlign: 'left',
                                                textDecorationLine: this.hasTrue() ? this.getArchivedIndex(this.state.item) === -1 ? 'none' : 'line-through' : this.state.item.archived ? 'line-through' : 'none',
                                                fontSize: 17,
                                                fontFamily: 'avenirroman',
                                                paddingTop: 3,
                                                paddingRight: 10,
                                                marginTop: 14,
                                                marginBottom: 14,
                                                lineHeight: 20
                                            }}>{this.state.item.name[this.state.item.name.length - 1] === ' ' ? this.state.item.name : this.state.item.name + " "} {(this.state.item.note || (underTask && underTask.length > 0)) && <Image source={this.state.showMore ? require('../assets/bubble_up.png') : require('../assets/bubble_down.png')} resizeMode="contain" style={{ height: 12, width: 12, alignSelf: 'center' }} />}</ThemedText>

                                        )}
                                </TouchableOpacity>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={100} onPress={() => this.archive()} onLongPress={() => this.delete()} style={{ position: 'absolute', right: 0, top: 0 }}>
                                <View style={{ height: 50, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                                    {this.hasTrue() ? this.getArchivedIndex(this.state.item) === -1 ? <NotArchived color={"#000"} /> : <Archived /> : this.state.item.archived ? <Archived /> : <NotArchived color={"#000"} />}
                                </View>
                                {/* <Image
                                    source={this.state.textEditing ? require('../assets/sendbig.png') : this.hasTrue() ? this.getArchivedIndex(this.state.item) === -1 ? require('../assets/notarchivedbig.png') : require('../assets/archivedbig.png') : this.state.item.archived ? require('../assets/archivedbig.png') : require('../assets/notarchivedbig.png')}
                                    resizeMode={"contain"} style={{ height: 50, width: 50 }} /> */}
                            </TouchableOpacity>
                            {this.state.showMore && (
                                <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => this.changeLayout()}>
                                    {underTask && underTask.length > 0 &&
                                        <FlatList data={underTask} style={{ width: '80%' }} keyExtractor={item => item.id.toString()} renderItem={({ item, index }) => {
                                            return (
                                                <TouchableOpacity onPress={() => {
                                                    underTask[index].archived = !underTask[index].archived;
                                                    this._isMounted && this.setState({ item: { ...this.state.item, underTask } });
                                                    this.endEditing();
                                                }} key={index} style={{ flexDirection: 'row', marginHorizontal: 20, marginTop: 10 }}>
                                                    {/* <Image source={item.archived ? require('../assets/underarchived.png') : require('../assets/not_archived.png')} style={{ height: 17, width: 17 }} resizeMode={"contain"} /> */}
                                                    {item.archived ? <UnderArchived /> : <UnderNotArchived color={"#000"} />}
                                                    <Text style={{
                                                        paddingTop: 3,
                                                        textAlign: 'left',
                                                        fontSize: 15,
                                                        lineHeight: 18,
                                                        fontFamily: 'avenirbook',
                                                        marginLeft: 10
                                                    }}>{item.name}</Text>
                                                </TouchableOpacity>
                                            );
                                        }} />
                                    }
                                    {this.state.item.note !== null && this.state.item.note.length > 0 &&
                                        <Text style={{
                                            marginTop: 16,
                                            marginLeft: 20,
                                            paddingRight: 10,
                                            textAlign: 'left',
                                            fontSize: 15,
                                            lineHeight: 18,
                                            width: '80%',
                                            fontFamily: 'avenirbook',
                                        }}>{this.state.item.note}</Text>
                                    }
                                    <TouchableOpacity activeOpacity={100} onPress={() => this.changeLayoutDrop()} style={{ alignItems: 'center', justifyContent: 'center', height: 50, width: '100%' }}>
                                        {/* <Image source={require('../assets/3hdots.png')} style={{ height: 25, width: 25, marginTop: 5 }} resizeMode={"contain"} /> */}
                                        <View style={{ height: 25, width: 25, marginTop: 5 }}>
                                            <ThreeDots color={"#000"} />
                                        </View>
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            )}
                            {this.state.showMoreAdd && (
                                <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => this.changeLayoutAdd()}>
                                    <View style={{ marginLeft: 20 }}>
                                        <Separator />
                                        <TouchableOpacity activeOpacity={100} onPress={() => null} onLongPress={() => null}>
                                            <FlatList data={underTask} keyExtractor={item => item.id.toString()} ListFooterComponent={() => {
                                                return (
                                                    <View style={styles.underTaskView}>
                                                        <ThemedTextInput placeholder={translate("addSubTask")} onEndEditing={(e) => this.addNewUnderTask(e)} style={styles.underTaskInput} />
                                                    </View>
                                                );
                                            }} renderItem={({ item }) => {
                                                return (
                                                    <View style={styles.underTaskView}>
                                                        {/* <Image source={require('../assets/not_archived.png')} style={styles.archivedImg} resizeMode={"contain"} /> */}
                                                        <UnderNotArchived color={"#000"} />
                                                        <ThemedText style={[styles.underTaskText, styles.paddingLeft10]}>{item.name}</ThemedText>
                                                    </View>
                                                );
                                            }} />
                                        </TouchableOpacity>
                                        <Separator />
                                        <View style={styles.row}>
                                            <ThemedTextInput multiline placeholder={translate("addNote")} onEndEditing={() => this.endEditing()} onChangeText={(e) => this._isMounted && this.setState({ item: { ...this.state.item, note: e } })} style={styles.noteInput} value={note} />
                                            {String(name).length > 0 && (
                                                <TouchableOpacity onPress={() => this.changeLayoutAdd()} style={styles.okView}>
                                                    <Text style={styles.ok}>OK</Text>
                                                </TouchableOpacity>
                                            )}
                                        </View>
                                        <Separator />
                                    </View>
                                    <View style={[styles.bigTagView, { padding: 10, flexDirection: 'row', flexWrap: "wrap" }]}>
                                        {this.props.tags.map((data, index) => {
                                            return (
                                                <TagAdd key={index} index={index} remove={() => this.setState({ removeTag: true, tagIndex: index })} setColor={(color) => { this.setState({ newColor: color }); this.props.editTag(this.state.newTag, color, this.state.newId, index); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} openTag={this.state.openTag} onLayout={(event) => this.setState({ openTagPosition: event.nativeEvent.layout })} onLongPress={() => { this.setState({ openTag: index, newTag: data.name, newColor: data.color, newId: data.id }); ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy) }} onPress={() => this.addTag(this.state.openTag === index ? this.state.newId : data.id)} id={this.state.openTag === index ? this.state.newId : data.id} color={this.state.openTag === index ? this.state.newColor : data.color} text={this.state.openTag === index ? this.state.newTag : data.name} tag={tag} onEndEditing={(e) => { this.props.editTag(this.state.newTag, this.state.newColor, this.state.newId, index); this.setState({ addTag: false }); }} onChangeText={(text) => this.setState({ newTag: text })} />
                                            );
                                        })}
                                        {this.state.addTag ? (
                                            <View style={{ backgroundColor: '#4E4E4E', borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                                                <TextInput autoFocus adjustsFontSizeToFit onEndEditing={(e) => { this.props.addTag(this.state.newTag, '#4E4E4E'); this.setState({ addTag: false, newTag: null }); }} onChangeText={(text) => this.setState({ newTag: text })} value={this.state.newTag} style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: 'avenirbook', marginTop: 5 }} />
                                            </View>
                                        ) : (
                                                <TouchableOpacity onPress={() => this.setState({ addTag: true, newColor: null, newTag: null, newId: null, openTag: null })} style={{ height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                                                    <ThemedText style={{ fontSize: 26 }}>+</ThemedText>
                                                </TouchableOpacity>
                                            )}
                                        {this.state.openTag !== null && this.state.openTag !== undefined && this.state.openTag >= 0 && (
                                            <View style={{ position: 'absolute', left: 10, top: this.state.openTagPosition.y - this.state.openTagPosition.height - 9, height: 40, width: 280, backgroundColor: '#F1F1F1', borderRadius: 25, flexDirection: 'row', alignItems: 'center' }}>
                                                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                                    <TouchableOpacity onPress={() => { this.setState({ newColor: '#F35B5B' }); this.props.editTag(this.state.newTag, '#F35B5B', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#F35B5B', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                    <TouchableOpacity onPress={() => { this.setState({ newColor: '#FFBE26' }); this.props.editTag(this.state.newTag, '#FFBE26', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#FFBE26', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                    <TouchableOpacity onPress={() => { this.setState({ newColor: '#16B476' }); this.props.editTag(this.state.newTag, '#16B476', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#16B476', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                    <TouchableOpacity onPress={() => { this.setState({ newColor: '#0094FF' }); this.props.editTag(this.state.newTag, '#0094FF', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#0094FF', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                    <TouchableOpacity onPress={() => { this.setState({ newColor: '#B652E5' }); this.props.editTag(this.state.newTag, '#B652E5', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#B652E5', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                    <TouchableOpacity onPress={() => { this.setState({ newColor: '#4E4E4E' }); this.props.editTag(this.state.newTag, '#4E4E4E', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#4E4E4E', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                </ScrollView>
                                                <TouchableOpacity onPress={() => this.setState({ removeTag: true, tagIndex: this.state.openTag })} style={{ width: 30, height: 30, marginHorizontal: 5 }}>
                                                    <Image source={require('../assets/blackbin2.png')} resizeMode={"contain"} style={{ width: 30, height: 30 }} />
                                                </TouchableOpacity>
                                                <TouchableOpacity onPress={() => this.setState({ openTag: null, newTag: null, newColor: null, newId: null, addTag: false })} style={{ width: 30, height: 30, marginHorizontal: 5, paddingRight: 5, alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../assets/removetag.png')} resizeMode={"contain"} style={{ width: 15, height: 15 }} />
                                                </TouchableOpacity>
                                            </View>
                                        )}
                                    </View>
                                    <TouchableOpacity activeOpacity={100} onPress={() => this.changeLayoutDrop()} style={{ alignItems: 'center', justifyContent: 'center', height: 50, width: '100%', marginTop: -20, marginLeft: -10 }}>
                                        {/* <Image source={require('../assets/3hdots.png')} style={{ height: 25, width: 25, marginTop: 5 }} resizeMode={"contain"} /> */}
                                        <View style={{ height: 25, width: 25, marginTop: 5 }}>
                                            <ThreeDots color={"#000"} />
                                        </View>
                                    </TouchableOpacity>
                                </TouchableOpacity>
                            )}
                            {this.state.showMoreDrop && (
                                <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => null}>
                                    <View style={{ marginHorizontal: 20 }}>
                                        <Separator />
                                        <FlatList scrollEnabled={false} data={underTask} keyExtractor={item => item.id.toString()} ListFooterComponent={() => {
                                            return (
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <ThemedTextInput numberOfLines={1} placeholder={translate("addSubTask")} onEndEditing={(e) => this.addNewUnderTask(e)} style={{ height: 40, fontFamily: 'avenirbook', fontSize: 17, width: '90%' }} />
                                                </View>
                                            );
                                        }} renderItem={({ item, index }) => {
                                            return (
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <ThemedTextInput numberOfLines={1} placeholder={translate("addSubTask")} onChange={(e) => this.editUnderTask(e, index)} onEndEditing={(e) => null} style={{ height: 40, fontFamily: 'avenirbook', fontSize: 17, width: '80%' }} value={item.name} />
                                                    <TouchableOpacity activeOpacity={100} onPress={() => this.removeUnderTask(index)}>
                                                        {/* <Image source={require('../assets/cross.png')} style={{ height: 17, width: 17 }} resizeMode={"contain"} /> */}
                                                        <RemoveUnder color={"#fff"} />
                                                    </TouchableOpacity>
                                                </View>
                                            );
                                        }} />
                                        <Separator />
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ width: '90%' }}>
                                                <ThemedTextInput multiline placeholder={translate("addNote")} onEndEditing={() => this.endEditing()} onChangeText={(e) => this._isMounted && this.setState({ item: { ...this.state.item, note: e } })} style={{ minHeight: 45, paddingTop: 14, paddingBottom: 10, width: '80%', textAlign: 'left', fontSize: 17, fontFamily: 'avenirbook', }} value={note} />
                                            </View>
                                            <TouchableOpacity activeOpacity={100} onPress={() => { this.props.update(); this.changeLayoutDrop() }} style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                <Text numberOfLines={1} style={{
                                                    textAlign: 'center',
                                                    fontSize: 18,
                                                    color: '#FECE2F',
                                                    fontFamily: 'avenirblack'
                                                }} adjustsFontSizeToFit>OK</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <Separator />
                                        <TouchableOpacity style={{ marginTop: 14, marginBottom: this.state.showMoreTag ? 0 : 10 }} activeOpacity={100} onPress={async () => {
                                            this._isMounted && this.setState({ showMoreTag: !this.state.showMoreTag });
                                            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                                            if (await AsyncStorage.getItem('YellowTaskShowMoreTag'))
                                                await AsyncStorage.removeItem('YellowTaskShowMoreTag')
                                            else
                                                await AsyncStorage.setItem('YellowTaskShowMoreTag', "true");
                                        }}>
                                            <ThemedText style={{ fontSize: 17 }}><HashTag color={"#000"} />  ID  <Image source={this.state.showMoreTag ? require('../assets/bubble_up.png') : require('../assets/bubble_down.png')} resizeMode="contain" style={{ height: 12, width: 12, alignSelf: 'center' }} /></ThemedText>
                                        </TouchableOpacity>
                                    </View>
                                    {this.state.showMoreTag && (
                                        <View style={[styles.bigTagView, { padding: 10, flexDirection: 'row', flexWrap: "wrap" }]}>
                                            {this.props.tags.map((data, index) => {
                                                return (
                                                    <TagAdd key={index} index={index} remove={() => this.setState({ removeTag: true, tagIndex: index })} setColor={(color) => { this.setState({ newColor: color }); this.props.editTag(this.state.newTag, color, this.state.newId, index); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} openTag={this.state.openTag} onLayout={(event) => this.setState({ openTagPosition: event.nativeEvent.layout })} onLongPress={() => { this.setState({ openTag: index, newTag: data.name, newColor: data.color, newId: data.id }); ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy) }} onPress={() => this.addTag(this.state.openTag === index ? this.state.newId : data.id)} id={this.state.openTag === index ? this.state.newId : data.id} color={this.state.openTag === index ? this.state.newColor : data.color} text={this.state.openTag === index ? this.state.newTag : data.name} tag={tag} onEndEditing={(e) => { this.props.editTag(this.state.newTag, this.state.newColor, this.state.newId, index); this.setState({ addTag: false }); }} onChangeText={(text) => this.setState({ newTag: text })} />
                                                );
                                            })}
                                            {this.state.addTag ? (
                                                <View style={{ backgroundColor: '#4E4E4E', borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                                                    <ThemedTextInput autoFocus adjustsFontSizeToFit onEndEditing={(e) => { this.props.addTag(this.state.newTag, '#4E4E4E'); this.setState({ addTag: false, newTag: null }); }} onChangeText={(text) => this.setState({ newTag: text })} value={this.state.newTag} style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: 'avenirbook', marginTop: 5 }} />
                                                </View>
                                            ) : (
                                                    <TouchableOpacity onPress={() => this.setState({ addTag: true, newColor: null, newTag: null, newId: null, openTag: null })} style={{ height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                                                        <ThemedText style={{ fontSize: 26 }}>+</ThemedText>
                                                    </TouchableOpacity>
                                                )}
                                            {this.state.openTag !== null && this.state.openTag !== undefined && this.state.openTag >= 0 && (
                                                <View style={{ position: 'absolute', left: 10, top: this.state.openTagPosition.y - this.state.openTagPosition.height - 9, height: 40, width: 280, backgroundColor: '#F1F1F1', borderRadius: 25, flexDirection: 'row', alignItems: 'center' }}>
                                                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#F35B5B' }); this.props.editTag(this.state.newTag, '#F35B5B', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#F35B5B', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#FFBE26' }); this.props.editTag(this.state.newTag, '#FFBE26', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#FFBE26', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#16B476' }); this.props.editTag(this.state.newTag, '#16B476', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#16B476', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#0094FF' }); this.props.editTag(this.state.newTag, '#0094FF', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#0094FF', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#B652E5' }); this.props.editTag(this.state.newTag, '#B652E5', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#B652E5', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#4E4E4E' }); this.props.editTag(this.state.newTag, '#4E4E4E', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#4E4E4E', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                    </ScrollView>
                                                    <TouchableOpacity onPress={() => this.setState({ removeTag: true, tagIndex: this.state.openTag })} style={{ width: 30, height: 30, marginHorizontal: 5 }}>
                                                        <Image source={require('../assets/blackbin2.png')} resizeMode={"contain"} style={{ width: 30, height: 30 }} />
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.setState({ openTag: null, newTag: null, newColor: null, newId: null, addTag: false })} style={{ width: 30, height: 30, marginHorizontal: 5, paddingRight: 5, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={require('../assets/removetag.png')} resizeMode={"contain"} style={{ width: 15, height: 15 }} />
                                                    </TouchableOpacity>
                                                </View>
                                            )}
                                        </View>
                                    )}
                                    {/* <Separator /> */}
                                    {/* <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontFamily: 'avenirbook', fontSize: 14, color: '#000', marginVertical: 5, paddingTop: 10, width: '90%' }}>Habitude :</Text>
                                            <TouchableOpacity activeOpacity={100} onPress={() => { this.props.update(); this.changeLayoutDrop() }} style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 5, paddingTop: 10 }}>
                                                <Text numberOfLines={1} style={{
                                                    textAlign: 'center',
                                                    fontSize: 18,
                                                    color: '#FECE2F',
                                                    fontFamily: 'avenirblack'
                                                }} adjustsFontSizeToFit>OK</Text>
                                            </TouchableOpacity>
                                        </View> */}
                                    {/* <View>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ flex: 1 }}>
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#FECE2F'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('monday', repeat ? !repeat.monday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.monday : false}
                                                        rightText={"Lundi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#FECE2F'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('tuesday', repeat ? !repeat.tuesday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.tuesday : false}
                                                        rightText={"Mardi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#FECE2F'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('wednesday', repeat ? !repeat.wednesday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.wednesday : false}
                                                        rightText={"Mercredi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#FECE2F'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('thursday', repeat ? !repeat.thursday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.thursday : false}
                                                        rightText={"Jeudi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                </View>
                                                <View style={{ flex: 1 }}>
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#FECE2F'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('friday', repeat ? !repeat.friday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.friday : false}
                                                        rightText={"Vendredi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#FECE2F'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('saturday', repeat ? !repeat.saturday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.saturday : false}
                                                        rightText={"Samedi"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                    <CheckBox
                                                        checkBoxColor='#000'
                                                        checkedCheckBoxColor='#FECE2F'
                                                        style={{ paddingVertical: 10 }}
                                                        onClick={() => {
                                                            this.addRepeat('sunday', repeat ? !repeat.sunday : true);
                                                        }}
                                                        isChecked={repeat ? repeat.sunday : false}
                                                        rightText={"Dimanche"}
                                                        rightTextStyle={{
                                                            color: '#000',
                                                            fontFamily: 'avenirbook',
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                        {this.hasTrue() && (
                                            <TouchableOpacity activeOpacity={100} onPress={() => this.props.deleteBubble(this.state.item.ref)} style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <Image source={require('../assets/blackbin2.png')} style={{ height: 20, width: 20, marginVertical: 10 }} resizeMode={"contain"} />
                                            </TouchableOpacity>
                                        )}*/}
                                    <View style={{ marginHorizontal: 20 }}>
                                        <Separator />
                                        <TouchableOpacity style={{ marginTop: 12, marginBottom: this.state.showMoreTag ? 10 : 20, flexDirection: 'row' }} activeOpacity={100} onPress={async () => {
                                            this._isMounted && this.setState({ showMoreReminder: !this.state.showMoreReminder });
                                            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                                            if (await AsyncStorage.getItem('YellowTaskShowMoreReminder'))
                                                await AsyncStorage.removeItem('YellowTaskShowMoreReminder')
                                            else
                                                await AsyncStorage.setItem('YellowTaskShowMoreReminder', "true");
                                        }}>
                                            {/* <ImageSVG source={require('../assets/reminder.svg')} style={{ width: 17, height: 17 }} /> */}
                                            <ThemedText style={{ fontSize: 17 }}><Reminder color={"#000"} />  {translate('reminder')}  <Image source={this.state.showMoreReminder ? require('../assets/bubble_up.png') : require('../assets/bubble_down.png')} resizeMode="contain" style={{ height: 12, width: 12, alignSelf: 'center' }} /></ThemedText>
                                        </TouchableOpacity>
                                        {this.state.showMoreReminder && (
                                            <View style={{ marginBottom: 10 }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    {Platform.OS === 'ios' ? <DateTimePicker
                                                        testID="dateTimePicker"
                                                        mode={'datetime'}
                                                        locale={'fr'}
                                                        is24Hour={true}
                                                        display="default"
                                                        onChange={(event, date) => this._isMounted && this.setState({ date, dateActivate: false })}
                                                        value={this.state.date}
                                                        style={{ height: 50, width: (width * 0.6) - 10, marginTop: 15, marginBottom: 10 }}
                                                    /> : this.state.dateActivate ?
                                                            (
                                                                <>
                                                                    <DateTimePicker
                                                                        testID="dateTimePicker"
                                                                        mode={'datetime'}
                                                                        locale={'fr'}
                                                                        is24Hour={true}
                                                                        display="default"
                                                                        onChange={(event, date) => this._isMounted && this.setState({ date, dateActivate: false })}
                                                                        value={this.state.date}
                                                                        style={{ width: (width * 0.6) - 10, marginLeft: 10 }}
                                                                    />
                                                                </>)
                                                            : <Button title="Sélectionner une date" style={{ width: (width * 0.6) - 10, marginLeft: 10 }} onPress={() => this._isMounted && this.setState({ dateActivate: true })} />}
                                                    <TouchableOpacity activeOpacity={100} onPress={() => this.addReminder()} style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                        <ThemedText numberOfLines={1} style={{
                                                            textAlign: 'center',
                                                            fontSize: 22,
                                                            fontFamily: 'avenirblack'
                                                        }} adjustsFontSizeToFit>+</ThemedText>
                                                    </TouchableOpacity>
                                                </View>
                                                {this.state.item.reminders && this.state.item.reminders.length > 0 && (
                                                    this.state.item.reminders.map((data, index) => (
                                                        <View key={index} style={{
                                                            height: 35,
                                                            flexDirection: 'row',
                                                            alignItems: 'center'
                                                        }}>
                                                            <ThemedText numberOfLines={1} style={{
                                                                textAlign: 'center',
                                                                marginTop: 7,
                                                                fontSize: 20,
                                                                lineHeight: 23,
                                                                fontFamily: 'avenirbook'
                                                            }}
                                                                adjustsFontSizeToFit>{moment(new Date(data)).format('lll')}</ThemedText>
                                                            <TouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(index)} style={{ height: 30, width: 30, paddingTop: 5 }}><Image
                                                                source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                                style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>
                                                        </View>
                                                    ))
                                                    // <View style={{ height: 150, flexDirection: 'row' }}>
                                                    //     <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    //         {this.state.startingIndex !== 0 && <TouchableOpacity activeOpacity={100} onPress={() => this._isMounted && this.setState({ startingIndex: this.state.startingIndex - 3 })}><Image source={require('../assets/less.png')} resizeMode={"contain"}
                                                    //             style={{ height: 20, width: 20, marginRight: 20 }} /></TouchableOpacity>}
                                                    //     </View>
                                                    //     <View style={{ width: '60%', flexDirection: 'column' }}>
                                                    //         {this.state.item.reminders && this.state.item.reminders[this.state.startingIndex] && (
                                                    //             <View style={{
                                                    //                 height: 50,
                                                    //                 justifyContent: 'center',
                                                    //                 alignItems: 'center',
                                                    //                 flexDirection: 'row'
                                                    //             }}>
                                                    //                 <Text numberOfLines={1} style={{
                                                    //                     textAlign: 'center',
                                                    //                     marginTop: 7,
                                                    //                     fontSize: 20,
                                                    //                     color: '#000',
                                                    //                     fontFamily: 'avenirbook'
                                                    //                 }}
                                                    //                     adjustsFontSizeToFit>{moment(new Date(this.state.item.reminders[this.state.startingIndex])).format('lll')}</Text>
                                                    //                 <TouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(this.state.startingIndex)} style={{ height: 30, width: 30 }}><Image
                                                    //                     source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                    //                     style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>
                                                    //             </View>
                                                    //         )}
                                                    //         {this.state.item.reminders && this.state.item.reminders[this.state.startingIndex + 1] && (
                                                    //             <View style={{
                                                    //                 height: 50,
                                                    //                 justifyContent: 'center',
                                                    //                 alignItems: 'center',
                                                    //                 flexDirection: 'row'
                                                    //             }}>
                                                    //                 <Text numberOfLines={1} style={{
                                                    //                     textAlign: 'center',
                                                    //                     marginTop: 7,
                                                    //                     fontSize: 20,
                                                    //                     color: '#000',
                                                    //                     fontFamily: 'avenirbook'
                                                    //                 }}
                                                    //                     adjustsFontSizeToFit>{moment(new Date(this.state.item.reminders[this.state.startingIndex + 1])).format('lll')}</Text>
                                                    //                 <TouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(this.state.startingIndex + 1)} style={{ height: 30, width: 30 }}><Image
                                                    //                     source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                    //                     style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>
                                                    //             </View>
                                                    //         )}
                                                    //         {this.state.item.reminders && this.state.item.reminders[this.state.startingIndex + 2] && (
                                                    //             <View style={{
                                                    //                 height: 50,
                                                    //                 justifyContent: 'center',
                                                    //                 alignItems: 'center',
                                                    //                 flexDirection: 'row'
                                                    //             }}>
                                                    //                 <Text numberOfLines={1} style={{
                                                    //                     textAlign: 'center',
                                                    //                     marginTop: 7,
                                                    //                     fontSize: 20,
                                                    //                     color: '#000',
                                                    //                     fontFamily: 'avenirbook'
                                                    //                 }}
                                                    //                     adjustsFontSizeToFit>{moment(new Date(this.state.item.reminders[this.state.startingIndex + 2])).format('lll')}</Text>
                                                    //                 <TouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(this.state.startingIndex + 2)} style={{ height: 30, width: 30 }}><Image
                                                    //                     source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                    //                     style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>
                                                    //             </View>
                                                    //         )}
                                                    //     </View>
                                                    //     <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center' }}>
                                                    //         {this.state.item.reminders && this.state.startingIndex + 3 < this.state.item.reminders.length && <TouchableOpacity activeOpacity={100} onPress={() => this._isMounted && this.setState({ startingIndex: this.state.startingIndex + 3 })}><Image source={require('../assets/more.png')} resizeMode={"contain"}
                                                    //             style={{ height: 20, width: 20, marginLeft: 20 }} /></TouchableOpacity>}
                                                    //     </View>
                                                    // </View>
                                                )}
                                            </View>
                                        )}
                                        <TouchableOpacity onPress={() => this.delete()}>
                                            <Text style={{ fontSize: 17, marginBottom: 20, color: '#F35B5B' }}><Image resizeMode="contain" source={require('../assets/bin.png')} style={{ height: 20, width: 20 }} />  {translate('removeTask')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableOpacity>
                            )}
                    </ThemedView>
                    {
                        this.state.error && (
                            <Overlay isVisible={true} onBackdropPress={() => this._isMounted && this.setState({ error: false })}
                                overlayBackgroundColor={'rgba(0, 0, 0, 0)'} overlayStyle={{
                                    width: width * 0.8,
                                    height: 120,
                                    backgroundColor: '#FFF',
                                    bottom: '5%',
                                    borderRadius: 15,
                                    opacity: 1,
                                }}>
                                <Text numberOfLines={1} style={{
                                    paddingHorizontal: 20,
                                    textAlign: 'left',
                                    fontSize: 18,
                                    lineHeight: 21,
                                    color: '#000',
                                    fontFamily: 'avenirblack'
                                }} adjustsFontSizeToFit>{translate("sorry")}</Text>
                                <Text numberOfLines={2} style={{
                                    paddingHorizontal: 20,
                                    textAlign: 'left',
                                    fontSize: 18,
                                    lineHeight: 21,
                                    color: '#C4C4C4',
                                    fontFamily: 'avenirbook',
                                    marginTop: 20
                                }}
                                    adjustsFontSizeToFit>{translate("cantArchive") + " " + this.props.date}</Text>
                            </Overlay>
                        )
                    }
                    {
                        this.state.removeTag && (
                            <Overlay isVisible={true} onBackdropPress={() => this._isMounted && this.setState({ removeTag: false, openTag: null, newTag: null, newColor: null, newId: null, addTag: false })}
                                overlayBackgroundColor={'rgba(0, 0, 0, 0)'} overlayStyle={{
                                    width: width * 0.8,
                                    height: 160,
                                    backgroundColor: '#FFF',
                                    bottom: '5%',
                                    borderRadius: 15,
                                    opacity: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginTop: 10
                                }}>
                                <>
                                    <Text style={{
                                        lineHeight: 20,
                                        paddingHorizontal: 5,
                                        textAlign: 'left',
                                        fontSize: 18,
                                        color: '#C4C4C4',
                                        fontFamily: 'avenirbook',
                                        marginTop: 5
                                    }}>{translate("removeTag")}</Text>
                                    <TouchableOpacity onPress={() => { this.props.removeTag(this.state.tagIndex); this.setState({ removeTag: false, openTag: null, newTag: null, newColor: null, newId: null, addTag: false }) }} style={{ backgroundColor: '#F35B5B', borderRadius: 10, height: 45, width: '100%', marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ textAlign: 'center', fontSize: 17, lineHeight: 20, color: '#FFFFFF', fontFamily: 'avenirbook', marginTop: 3 }}>{translate("delete")}</Text>
                                    </TouchableOpacity>
                                </>
                            </Overlay>
                        )
                    }
                </View >
            );
        } else
            return (<View />);
    }
}

const styles = StyleSheet.create({
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 15,
        elevation: 15
    },
    blurView: {
        width: width * 0.8,
        backgroundColor: 'rgba(255, 255, 255, 0.95)',
        borderRadius: 25,
        borderColor: '#dcdcdc',
    },
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 7,
    },
    cancel: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        lineHeight: 20,
        fontFamily: 'avenirbook',
    },
    cancelView: {
        minHeight: 45,
        width: '40%',
        justifyContent: 'center',
    },
    ok: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        lineHeight: 20,
        fontFamily: 'avenirblack',
        color: '#FECE2F'
    },
    okView: {
        width: '20%',
        minHeight: 45,
        justifyContent: 'center',
    },
    taskInput: {
        minHeight: 45,
        width: '60%',
        textAlign: 'left',
        fontSize: 17,
        lineHeight: 20,
        fontFamily: 'avenirbook',
    },
    underTaskInput: {
        minHeight: 45,
        paddingTop: 3,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        lineHeight: 18,
        fontFamily: 'avenirbook',
    },
    underTaskText: {
        minHeight: 45,
        paddingTop: 16,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        lineHeight: 18,
        fontFamily: 'avenirbook',
    },
    noteInput: {
        minHeight: 45,
        paddingTop: 14,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        lineHeight: 18,
        fontFamily: 'avenirbook',
    },
    row: {
        flexDirection: 'row',
    },
    tagView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    bigTagView: {
        paddingTop: 10,
    },
    space: {
        height: 10
    },
    underTaskView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    archivedImg: {
        height: 17,
        width: 17,
    },
    paddingLeft10: {
        paddingLeft: 10,
    }
});

function Separator() {
    return (
        <View style={{ width: '60%', height: 1, backgroundColor: 'rgba(0, 0, 0, 0.1)' }} />
    );
}

function Tag(props) {
    return (
        <View>
            <Text adjustsFontSizeToFit style={{ letterSpacing: 2, color: props.color, paddingHorizontal: 10, textAlign: 'center', fontSize: 15, lineHeight: 18, fontFamily: 'avenirblack', marginTop: 5 }}>{props.text}</Text>
        </View>
    );
}

function hasTag(id, tag) {
    for (let i in tag) {
        if (tag[i] === id)
            return true;
    }
    return false;
}

function TagAdd(props) {
    return (
        <>
            {props.openTag === props.index ? (
                <View onLayout={props.onLayout} style={{ backgroundColor: props.color, borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                    <TextInput adjustsFontSizeToFit onEndEditing={props.onEndEditing} onChangeText={props.onChangeText} value={props.text} style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: 'avenirbook', marginTop: 5 }} />
                </View>
            ) : (
                    <TouchableOpacity onLongPress={props.onLongPress} onPress={props.onPress} style={{ backgroundColor: props.color, borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                        <Text adjustsFontSizeToFit style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, lineHeight: 18, fontFamily: hasTag(props.id, props.tag) ? 'avenirblack' : 'avenirbook', marginTop: 5 }}>{props.text}</Text>
                    </TouchableOpacity>
                )}
        </>
    );
}