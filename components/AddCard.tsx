import React, { useState, useRef, useEffect } from 'react';
import firebase from '../constants/firebase';
import { addCard } from '../api/Cards';
import { Alert, TouchableOpacity, View, TextInput, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { height, width } from '../constants/Layout';
import CheckBox from 'react-native-check-box';
import { translate } from '../locales/i18n';

export default function AddCard(props) {
    const [cardName, setCardName] = useState("");
    const [cardType, setCardType] = useState("");

    const cardField = useRef(null);

    const addNewCard = async () => {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            props.navigation.navigate('First');
            return;
        }
        if (cardName === undefined) {
            Alert.alert('Merci de donner un nom');
            return;
        }
        if (cardType === undefined) {
            Alert.alert('Merci de donner un type');
            return;
        }
        let res = await addCard({ cardName, cardType }, user, props.db, props.lists);
        props.setLists(res.lists);
        props.setSelected(res.selected);
        props.setAddCard(res.addCard);
        setCardName(res.cardName);
        setCardType(res.cardType);
        props.analytics.track("Add Card", { "None": "None" });
    }

    return (
        <>
            {props.visible && (
                <TouchableOpacity activeOpacity={100} onPress={props.hide} style={{
                    position: 'absolute',
                    height: height * 2,
                    width: width,
                    backgroundColor: 'rgba(0, 0, 0, 0.05)',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <View style={{
                        width: width * 0.8,
                        backgroundColor: '#FFF',
                        bottom: '5%',
                        justifyContent: 'center',
                        borderRadius: 15,
                        opacity: 1,
                    }}>
                        <KeyboardAwareScrollView scrollEnabled={false}>
                            <View style={{
                                paddingVertical: 20,
                                paddingHorizontal: 20,
                                justifyContent: 'center',
                                flex: 4
                            }}>
                                <View style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    borderBottomWidth: 0.5,
                                    borderBottomColor: '#C4C4C4'
                                }}>
                                    <TouchableOpacity activeOpacity={100} onPress={() => cardField.current.focus()} style={{ flex: 2, backgroundColor: '#fff' }}>
                                        <TextInput onSubmitEditing={addNewCard}
                                            value={cardName}
                                            onChangeText={setCardName}
                                            ref={cardField}
                                            style={{ width: '80%', fontSize: 20, fontFamily: 'avenirbook' }}
                                            placeholder={translate("giveMeAName")} />
                                    </TouchableOpacity>
                                    <View style={{ justifyContent: 'flex-end', marginTop: -15 }}>
                                        <TouchableOpacity activeOpacity={100} onPress={addNewCard} style={{
                                            width: '100%',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                            <Image style={{ height: 40, width: 40, marginTop: 4 }}
                                                resizeMode={"contain"} source={require('../assets/sendbig.png')} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <CheckBox
                                        checkBoxColor='#C4C4C4'
                                        checkedCheckBoxColor='#FECE2F'
                                        checkedImage={null}
                                        style={{ flex: 1, padding: 5, paddingTop: 20 }}
                                        onClick={() => setCardType('calendar')}
                                        isChecked={cardType === 'calendar'}
                                        rightText={translate("calendar")}
                                        rightTextStyle={{ color: '#C4C4C4', fontFamily: 'avenirbook', fontSize: 15 }}
                                    />
                                    <CheckBox
                                        checkBoxColor='#C4C4C4'
                                        checkedCheckBoxColor='#FECE2F'
                                        style={{ flex: 1, padding: 5 }}
                                        onClick={() => setCardType('perso')}
                                        isChecked={cardType === 'perso'}
                                        rightText={translate("custom")}
                                        rightTextStyle={{ color: '#C4C4C4', fontFamily: 'avenirbook', fontSize: 15 }}
                                    />
                                </View>
                            </View>
                        </KeyboardAwareScrollView>
                    </View>
                </TouchableOpacity>
            )}
        </>
    );
}