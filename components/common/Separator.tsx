import React from 'react';
import { View } from './Themed';
import { width } from '../../constants/Layout';

export default function Separator() {
    return (
        <View style={{
            height: 2.5,
            marginLeft: 30,
            width: width * 0.3,
            marginBottom: 10
        }} />
    );
}