import React, { Component } from 'react';
import { TouchableOpacity, View, Linking } from 'react-native';
import { Text as ThemedText, View as ThemedView } from './common/Themed';
import { SwipeablePanel } from 'rn-swipeable-panel';
import * as StoreReview from 'expo-store-review';
import { width } from '../constants/Layout';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import firebase from '../constants/firebase';
import { translate } from '../locales/i18n';

export default class SliddingMenu extends Component {

    uriToBlob = (uri) => {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.onload = function () {
                // return the blob
                resolve(xhr.response);
            };

            xhr.onerror = function () {
                // something went wrong
                reject(new Error('uriToBlob failed'));
            };
            // this helps us get a blob
            xhr.responseType = 'blob';
            xhr.open('GET', uri, true);

            xhr.send(null);
        });
    }

    uploadToFirebase = (blob) => {
        const user = firebase.auth().currentUser;
        return new Promise((resolve, reject) => {
            var storageRef = firebase.storage().ref();
            storageRef.child('backgrounds/' + user.uid).put(blob, {
                contentType: 'image/jpeg'
            }).then((snapshot) => {
                blob.close();
                resolve(snapshot);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    handleChoosePhoto = async () => {
        const gallery = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        const hasGalleryPermission = (gallery.status === 'granted');
        if (hasGalleryPermission === null || hasGalleryPermission === false) {
            Alert.alert("Merci d'autoriser l'accès à vos photos pour changer le fond d'écran");
            return;
        }
        ImagePicker.launchImageLibraryAsync({
            mediaTypes: "Images"
        }).then((result) => {
            if (!result.cancelled) {
                const { height, width, type, uri } = result;
                this.props.db.transaction(
                    tx => {
                        tx.executeSql("update users set background = (?)", [uri]);
                    },
                    null,
                );
                this.props.setImg(img);
                return this.uriToBlob(uri);
            }
        }).then((blob) => {
            return this.uploadToFirebase(blob);
        }).then((snapshot) => {
            console.log("File uploaded");
            this.props.analytics.track("Edit-Back", { "None": "None" });
        }).catch((error) => {
            throw error;
        });
    }

    signOut = async () => {
        this.props.db.transaction(tx => {
            tx.executeSql("drop table if exists users");
            tx.executeSql("drop table if exists lists");
            tx.executeSql("drop table if exists pages");
            tx.executeSql("drop table if exists bubbles");
        });
        this.props.navigation.navigate('First');
        await firebase.auth().signOut();
    };

    render() {
        return (
            <SwipeablePanel style={{ width: width * 0.9, borderRadius: 25 }} isActive={this.props.swipeablePanelActive} closeOnTouchOutside noBackgroundOpacity
                noBar onClose={() => this.props.closePanel()} onlyLarge>
                <ThemedView style={{ padding: 20, borderRadius: 25, height: '200%' }}>
                    <TouchableOpacity activeOpacity={100} onPress={() => this.handleChoosePhoto()}>
                            <ThemedText style={{
                                textAlign: 'left',
                                fontSize: 18,
                                lineHeight: 21,
                                fontFamily: 'avenirblack',
                                marginTop: 10,
                                marginBottom: 20
                            }} adjustsFontSizeToFit>→ {translate("background")}</ThemedText>
                        </TouchableOpacity>
                    <TouchableOpacity activeOpacity={100} onPress={() => Linking.openURL('https://yellow-task.webflow.io/contact')}>
                            <ThemedText style={{
                                textAlign: 'left',
                                fontSize: 18,
                                lineHeight: 21,
                                fontFamily: 'avenirblack',
                                marginTop: 20
                            }} adjustsFontSizeToFit>{translate("ideasForImprovements")}</ThemedText>
                            <ThemedText style={{
                                textAlign: 'left',
                                fontSize: 18,
                                lineHeight: 21,
                                fontFamily: 'avenirblack',
                                paddingBottom: 15,
                            }} adjustsFontSizeToFit>{translate("sendIdea")}</ThemedText>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={100} onPress={() => StoreReview.requestReview()}>
                            <ThemedText style={{
                                textAlign: 'left',
                                fontSize: 18,
                                lineHeight: 21,
                                fontFamily: 'avenirblack',
                                marginTop: 15
                            }} adjustsFontSizeToFit>{translate("fiveStars")}</ThemedText>
                            <ThemedText style={{
                                textAlign: 'left',
                                fontSize: 18,
                                lineHeight: 21,
                                fontFamily: 'avenirblack',
                                paddingBottom: 15,
                            }} adjustsFontSizeToFit>{translate("giveStars")}</ThemedText>
                        </TouchableOpacity>
                        <View>
                            <TouchableOpacity activeOpacity={100} onPress={() => this.signOut()}>
                                <ThemedText style={{
                                    textAlign: 'left',
                                    fontSize: 15,
                                    lineHeight: 18,
                                    fontFamily: 'avenirblack',
                                    paddingBottom: 20
                                }} adjustsFontSizeToFit>{translate("logout")}</ThemedText>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={100} onPress={() => Linking.openURL('https://www.yellowtask.co/mentions-legales')}>
                                <ThemedText style={{
                                    textAlign: 'left',
                                    fontSize: 15,
                                    lineHeight: 18,
                                    fontFamily: 'avenirblack',
                                    paddingBottom: 20
                                }} adjustsFontSizeToFit>{translate("legal")}</ThemedText>
                            </TouchableOpacity>
                        </View>
                </ThemedView>
            </SwipeablePanel>
        );
    }
}