import React from 'react';
import { Image, TextInput, FlatList, View, TouchableOpacity } from 'react-native';
import { TouchableOpacity as ThemedTouchableOpacity, Text as ThemedText } from './common/Themed';
import { translate } from '../locales/i18n';
import { width } from '../constants/Layout';
import { editCard, removeCard } from '../api/Cards';
import firebase from '../constants/firebase';

interface HeaderProps {
    analytics: any;
    lists: Array<any>;
    editing: boolean;
    selected: number;
    openPanel: any;
    db: any;
    setLists: any;
    setEditing: any;
    setSelected: any;
    setAddCard: any;
}

export default function Header(props: HeaderProps) {

    const removeSelectedCard = async () => {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        let res = removeCard(props.lists, props.selected, props.db);
        props.setLists(res.lists);
        props.setEditing(res.editing);
        props.setSelected(res.selected);
        props.analytics.track("Del Card", { "None": "None" });
    }

    const editList = (e, id) => {
        let { lists } = this.state;

        lists[id].name = e;
        props.setLists(lists);
    }

    const endEditingList = async (id) => {
        const user = firebase.auth().currentUser;

        if (user && !user.uid) {
            this.props.navigation.navigate('First');
            return;
        }
        let res = editCard(this.state, id, props.db);
        props.setEditing(res.editing);
        props.setSelected(res.selected);
        props.analytics.track("Edit Card", { "None": "None" });
    }

    const renderFooter = () => {
        return <TouchableOpacity activeOpacity={100} onPress={() => props.setAddCard(true)}><Image
            source={require('../assets/addcard.png')} resizeMode={"contain"}
            style={{ height: 20, width: 20, marginLeft: 20, marginRight: 20 }} /></TouchableOpacity>;
    };

    return (
        <View style={{ flexDirection: 'row', flex: 1 }}>
            <TouchableOpacity activeOpacity={100} onPress={() => props.openPanel()}
                style={{ justifyContent: 'center', marginRight: 10 }}>
                <Image source={require('../assets/menu.png')} resizeMode={"contain"}
                    style={{ height: 20, width: 20, marginRight: 10, marginLeft: 30 }} />
            </TouchableOpacity>
            {!props.lists || props.lists.length < 0 ? <View /> : (
                <FlatList style={{ flex: 3, width }} ListFooterComponent={renderFooter} horizontal
                    showsHorizontalScrollIndicator={false} data={props.lists}
                    keyExtractor={(item) => item.id.toString()}
                    contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }}
                    renderItem={({ item }) => (
                        <ThemedTouchableOpacity activeOpacity={100} key={item.id} onLongPress={() => {
                            props.setEditing(true);
                            props.setSelected(item.id);
                        }} onPress={() => {
                            props.setSelected(item.id);
                            props.analytics.track("Change-card", { "None": "None" });
                        }} style={{
                            marginHorizontal: 10,
                            justifyContent: 'center',
                            height: 50,
                            alignItems: 'center',
                            paddingHorizontal: 30,
                            borderRadius: 50,
                            borderColor: '#dcdcdc'
                        }}>
                            {props.editing && props.selected === item.id ? (
                                <View style={{
                                    justifyContent: 'center',
                                    height: 50,
                                    alignItems: 'center',
                                    flexDirection: 'row'
                                }}>
                                    <TextInput onEndEditing={() => endEditingList(item.id)}
                                        ref={"list" + item.id.toString()} value={item.name}
                                        onChangeText={e => editList(e, item.id)} style={{
                                            fontSize: 20,
                                            textAlign: 'center',
                                            marginTop: 3,
                                            fontFamily: 'avenirblack',
                                            marginRight: 20
                                        }} placeholder={translate("giveMeAName")} />
                                    <TouchableOpacity activeOpacity={100} onPress={() => removeSelectedCard()} style={{
                                        height: 50,
                                        paddingLeft: 20,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderLeftWidth: 1,
                                        borderLeftColor: '#C4C4C4'
                                    }}>
                                        <Image source={require('../assets/bin.png')}
                                            resizeMode={"contain"}
                                            style={{ height: 20, width: 20 }} />
                                    </TouchableOpacity>
                                    <TouchableOpacity activeOpacity={100} onPress={() => props.setEditing(false)} style={{
                                        height: 50,
                                        paddingLeft: 20,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                        <Image source={require('../assets/removetag.png')}
                                            resizeMode={"contain"}
                                            style={{ height: 20, width: 20 }} />
                                    </TouchableOpacity>
                                </View>
                            )
                                : <ThemedText style={{
                                    textAlign: 'center',
                                    fontSize: 20,
                                    lineHeight: 25,
                                    marginTop: 3,
                                    fontFamily: props.selected.toString() === item.id.toString() ? 'avenirblack' : 'avenirbook'
                                }} adjustsFontSizeToFit>{item.name}</ThemedText>}
                        </ThemedTouchableOpacity>
                    )} />
            )}
        </View>
    );
}