import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Image, Text, FlatList } from 'react-native';
import { height, width } from '../constants/Layout';
import EditBubbleBox from './EditBubbleBox';
import Bubble from "./Bubble";
import 'moment/locale/fr';
import * as SQLite from 'expo-sqlite';
import { hasTrue } from '../api/Lists';
import Separator from './common/Separator';
import { translate } from '../locales/i18n';

const db = SQLite.openDatabase('yellowtask.db');

export default class ListPerso extends Component {
    render() {
        return (
            <View style={{ marginRight: this.props.carouselItems.length === 1 ? width * 0.12 : 0 }}>
                <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
                    <View style={{ width: (width * 0.8) - 80 }}>
                        <TextInput style={{
                            textAlign: 'left',
                            fontSize: 22,
                            color: '#fff',
                            fontFamily: 'avenirbook'
                        }} onChangeText={(e) => this.props.editPageName(e)}
                            onEndEditing={() => this.props.editPage(this.props.item, this.props.index)}
                            maxLength={30}
                            placeholderTextColor={"#fff"}
                            placeholder={translate("addPage")}>{this.props.item.page}</TextInput>

                    </View>
                    <View style={{ width: 80, alignItems: 'center', paddingLeft: 70 }}>
                        <TouchableOpacity activeOpacity={100}
                            onPress={() => this.props.openPageDel()}>
                            <Image source={require('../assets/plus.png')}
                                resizeMode={"contain"}
                                style={{
                                    height: this.props.item.page ? 20 : 0,
                                    width: this.props.item.page ? 20 : 0
                                }} />
                        </TouchableOpacity>
                    </View>
                </View>
                <Separator />
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    style={{ height: height * 0.8 }}
                    data={this.props.carouselItems[this.props.index].bubbles}
                    extraData={this.props}
                    renderItem={({ item, index }) => <Bubble increaseTagIndex={this.props.increaseTagIndex} addTag={this.props.addTag} removeTag={this.props.removeTag} editTag={this.props.editTag} tags={this.props.tags} confettiRef={this.props.confettiRef} update={() => this.props.updateBubble()} analytics={this.props.analytics} item={item}
                             deleteBubble={(bubbleRef) => this.props.removeBubble(bubbleRef)} dataBase={db} />}
                    ListFooterComponent={(
                        <View>
                            <EditBubbleBox increaseTagIndex={this.props.increaseTagIndex} addTag={this.props.addTag} removeTag={this.props.removeTag} editTag={this.props.editTag} tags={this.props.tags} onAddBubble={(data) => this.props.addBubble(data)} />
                        </View>
                    )}
                />
            </View>
        );
    }
}