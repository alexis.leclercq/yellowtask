import React, { Component } from 'react';
import { View } from 'react-native';
import moment from "moment";
import ListPerso from './ListPerso';
import ListDay from './calendar/ListDay';
import ListWeek from './calendar/ListWeek';
import ListMonth from './calendar/ListMonth';
import { translate } from '../locales/i18n';

export default class List extends Component {

    render() {
        let day;
        let month;
        let type = this.props.type;
        let moment_date;
        if (type === "calendar") {
            if (this.props.item && this.props.item.date)
                moment_date = moment(new Date(this.props.item.date[3] + this.props.item.date[4] + "/" + this.props.item.date.slice(0, 3) + this.props.item.date.slice(6))).locale('en');
            else if (this.props.item && this.props.item.date_start)
                moment_date = moment(new Date(this.props.item.date_start[3] + this.props.item.date_start[4] + "/" + this.props.item.date_start.slice(0, 3) + this.props.item.date_start.slice(6))).locale('en');
            if (moment_date !== undefined) {
                day = translate(moment_date.format('dddd').toLowerCase());
                month = translate(moment_date.format('MMMM').toLowerCase());
            }
        }
        if (type === "perso")
            return <ListPerso increaseTagIndex={this.props.increaseTagIndex} addTag={this.props.addTag} removeTag={this.props.removeTag} editTag={this.props.editTag} carouselItems={this.props.carouselItems} tags={this.props.tags} editPageName={this.props.editPageName} editPage={this.props.editPage} item={this.props.item} index={this.props.index} openPageDel={this.props.openPageDel} confettiRef={this.props.confettiRef} updateBubble={this.props.updateBubble} removeBubble={this.props.removeBubble} analytics={this.props.analytics} addBubble={this.props.addBubble} />;
        if (this.props.time === 'day')
            return <ListDay increaseTagIndex={this.props.increaseTagIndex} addTag={this.props.addTag} removeTag={this.props.removeTag} editTag={this.props.editTag} day={day} month={month} carouselItems={this.props.carouselItems} tags={this.props.tags} item={this.props.item} index={this.props.index} confettiRef={this.props.confettiRef} updateBubble={this.props.updateBubble} removeBubble={this.props.removeBubble} analytics={this.props.analytics} addBubble={this.props.addBubble} />;
        else if (this.props.time === 'week')
            return <ListWeek increaseTagIndex={this.props.increaseTagIndex} addTag={this.props.addTag} removeTag={this.props.removeTag} editTag={this.props.editTag} day={day} month={month} carouselItems={this.props.carouselItems} tags={this.props.tags} item={this.props.item} index={this.props.index} confettiRef={this.props.confettiRef} updateBubble={this.props.updateBubble} removeBubble={this.props.removeBubble} analytics={this.props.analytics} addBubble={this.props.addBubble} />;
        else if (this.props.time === 'month')
            return <ListMonth increaseTagIndex={this.props.increaseTagIndex} addTag={this.props.addTag} removeTag={this.props.removeTag} editTag={this.props.editTag} day={day} month={month} carouselItems={this.props.carouselItems} tags={this.props.tags} item={this.props.item} index={this.props.index} confettiRef={this.props.confettiRef} updateBubble={this.props.updateBubble} removeBubble={this.props.removeBubble} analytics={this.props.analytics} addBubble={this.props.addBubble} />;
        else
            return <View />;
    }
}