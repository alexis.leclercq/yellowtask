import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { height, width } from '../../constants/Layout';
import EditBubbleBox from '../EditBubbleBox';
import Bubble from "../Bubble";
import moment from "moment";
import * as SQLite from 'expo-sqlite';
import { hasTrue } from '../../api/Lists';
import Separator from '../common/Separator';
import { translate } from '../../locales/i18n';
import { TextInverted as ThemedTextInverted } from '../common/Themed';

const db = SQLite.openDatabase('yellowtask.db');

export default function BubbleWeek(props) {

    const isToday = (date) => {
        return moment().locale('fr').format('L') === date;
    }

    const isTomorrow = (date) => {
        return moment().add("1", "days").locale('fr').format('L') === date;
    }

    const isNextTomorrow = (date) => {
        return moment().add("2", "days").locale('fr').format('L') === date;
    }

    const getFirstDate = (item, day) => {
        if (day) {
            if (isToday(item.date))
                return translate("today");
            else if (isTomorrow(item.date))
                return translate("tomorrow");
            else if (isNextTomorrow(item.date))
                return translate("afterTomorrow");
            else
                return day;
        }
    }

    const getSecondDate = (item, day, month) => {
        if (item && item.date && month)
            if (isToday(item.date) || isTomorrow(item.date) || isNextTomorrow(item.date))
                return day[0].toUpperCase() + day.slice(1, 3) + " " + item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
            else
                return item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
    }

    let day;
    let month;
    let moment_date;
    if (props.carouselItems && props.carouselItems[props.index] && props.carouselItems[props.index].days && props.carouselItems[props.index].days[props.bubbleIndex]) {
        moment_date = moment(new Date(props.carouselItems[props.index].days[props.bubbleIndex].date[3] + props.carouselItems[props.index].days[props.bubbleIndex].date[4] + "/" + props.carouselItems[props.index].days[props.bubbleIndex].date.slice(0, 3) + props.carouselItems[props.index].days[props.bubbleIndex].date.slice(6))).locale('en');
        day = translate(moment_date.format('dddd').toLowerCase());
        month = translate(moment_date.format('MMMM').toLowerCase());
        return (
            <>
                <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
                    <View style={{ width: 'auto' }}>
                        <ThemedTextInverted style={{
                            textAlign: 'left',
                            fontSize: 22,
                            lineHeight: 25,
                            fontFamily: 'avenirbook'
                        }}
                            adjustsFontSizeToFit>{getFirstDate(props.carouselItems[props.index].days[props.bubbleIndex], day)}</ThemedTextInverted>
                    </View>
                    <View style={{ width: 'auto', alignItems: 'center', paddingLeft: 0 }}>
                        <ThemedTextInverted style={{
                            textAlign: 'left',
                            paddingLeft: 10,
                            fontSize: 14,
                            lineHeight: 17,
                            fontFamily: 'avenirbook'
                        }}
                            adjustsFontSizeToFit>{getSecondDate(props.carouselItems[props.index].days[props.bubbleIndex], day, month)}</ThemedTextInverted>
                    </View>
                </View>
                <Separator />
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    style={{ marginBottom: 10 }}
                    data={props.carouselItems[props.index].days[props.bubbleIndex].bubbles}
                    extraData={props}
                    renderItem={({ item, index }) => {
                        if ((!item.repeat || !hasTrue(item.repeat)))
                            if (((item.date >= moment().locale('fr').subtract(1, "days").format('L') || item.archived !== true)))
                                return (
                                    <Bubble increaseTagIndex={props.increaseTagIndex} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef}
                                        update={() => props.updateBubble()}
                                        analytics={props.analytics} item={item}
                                        deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)}
                                        dataBase={db} />
                                );
                    }}
                    ListFooterComponent={(
                        <View>
                            <EditBubbleBox
                                increaseTagIndex={props.increaseTagIndex} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} onAddBubble={(data) => props.addBubble(data, props.carouselItems[props.index].days[props.bubbleIndex].date)} />
                            {props.item && props.item.repeated && props.item.repeated.length > 0 && (
                                <View>
                                    <View style={{
                                        flexDirection: 'row',
                                        paddingLeft: 30,
                                        alignItems: 'center'
                                    }}>
                                        <Text style={{
                                            textAlign: 'left',
                                            fontSize: 22,
                                            lineHeight: 25,
                                            color: '#fff',
                                            fontFamily: 'avenirbook'
                                        }}
                                            adjustsFontSizeToFit>Habitudes</Text>
                                    </View>
                                    <View style={{
                                        height: 1,
                                        marginLeft: 30,
                                        borderTopWidth: 2,
                                        borderTopColor: '#fff',
                                        width: width * 0.2,
                                        marginBottom: 20
                                    }} />
                                    {props.item.repeated.map((item2, index2) => {
                                        return <Bubble increaseTagIndex={props.increaseTagIndex} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef}
                                            update={() => props.updateBubble()}
                                            analytics={props.analytics} date={item2.date}
                                            key={index2}
                                            item={item2.bubble}
                                            deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)}
                                            dataBase={db} />;
                                    })}
                                </View>
                            )}
                        </View>
                    )}
                />
            </>
        );
    } else {
        return <View />;
    }
}

// export default class BubbleWeek extends Component {

//     constructor(props) {
//         super(props);
//     }

//     isToday(date) {
//         return moment().locale('fr').format('L') === date;
//     }

//     isTomorrow(date) {
//         return moment().add("1", "days").locale('fr').format('L') === date;
//     }

//     isNextTomorrow(date) {
//         return moment().add("2", "days").locale('fr').format('L') === date;
//     }

//     getFirstDate(item, day) {
//         if (day) {
//             if (this.isToday(item.date))
//                 return translate("today");
//             else if (this.isTomorrow(item.date))
//                 return translate("tomorrow");
//             else if (this.isNextTomorrow(item.date))
//                 return translate("afterTomorrow");
//             else
//                 return day;
//         }
//     }

//     getSecondDate(item, day, month) {
//         if (item && item.date && month)
//             if (this.isToday(item.date) || this.isTomorrow(item.date) || this.isNextTomorrow(item.date))
//                 return day[0].toUpperCase() + day.slice(1, 3) + " " + item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
//             else
//                 return item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
//     }

//     render() {
//         let day;
//         let month;
//         let moment_date;
//         if (props.carouselItems && props.carouselItems[props.index] && props.carouselItems[props.index].days && props.carouselItems[props.index].days[props.bubbleIndex]) {
//             moment_date = moment(new Date(props.carouselItems[props.index].days[props.bubbleIndex].date[3] + props.carouselItems[props.index].days[props.bubbleIndex].date[4] + "/" + props.carouselItems[props.index].days[props.bubbleIndex].date.slice(0, 3) + props.carouselItems[props.index].days[props.bubbleIndex].date.slice(6))).locale('en');
//             day = translate(moment_date.format('dddd').toLowerCase());
//             month = translate(moment_date.format('MMMM').toLowerCase());
//             return (
//                 <>
//                     <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
//                         <View style={{ width: 'auto' }}>
//                             <ThemedTextInverted style={{
//                                 textAlign: 'left',
//                                 fontSize: 22,
//                                 lineHeight: 25,
//                                 fontFamily: 'avenirbook'
//                             }}
//                                 adjustsFontSizeToFit>{this.getFirstDate(props.carouselItems[props.index].days[props.bubbleIndex], day)}</ThemedTextInverted>
//                         </View>
//                         <View style={{ width: 'auto', alignItems: 'center', paddingLeft: 0 }}>
//                             <ThemedTextInverted style={{
//                                 textAlign: 'left',
//                                 paddingLeft: 10,
//                                 fontSize: 14,
//                                 lineHeight: 17,
//                                 fontFamily: 'avenirbook'
//                             }}
//                                 adjustsFontSizeToFit>{this.getSecondDate(props.carouselItems[props.index].days[props.bubbleIndex], day, month)}</ThemedTextInverted>
//                         </View>
//                     </View>
//                     <Separator />
//                     <FlatList
//                         keyExtractor={(item, index) => index.toString()}
//                         showsVerticalScrollIndicator={false}
//                         style={{ marginBottom: 10 }}
//                         data={props.carouselItems[props.index].days[props.bubbleIndex].bubbles}
//                         extraData={props}
//                         renderItem={({ item, index }) => {
//                             if ((!item.repeat || !hasTrue(item.repeat)))
//                                 if (((item.date >= moment().locale('fr').subtract(1, "days").format('L') || item.archived !== true)))
//                                     return (
//                                         <Bubble increaseTagIndex={props.increaseTagIndex} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef}
//                                             update={() => props.updateBubble()}
//                                             analytics={props.analytics} item={item}
//                                             deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)}
//                                             dataBase={db} />
//                                     );
//                         }}
//                         ListFooterComponent={(
//                             <View>
//                                 <EditBubbleBox
//                                     increaseTagIndex={props.increaseTagIndex} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} onAddBubble={(data) => props.addBubble(data, props.carouselItems[props.index].days[props.bubbleIndex].date)} />
//                                 {props.item && props.item.repeated && props.item.repeated.length > 0 && (
//                                     <View>
//                                         <View style={{
//                                             flexDirection: 'row',
//                                             paddingLeft: 30,
//                                             alignItems: 'center'
//                                         }}>
//                                             <Text style={{
//                                                 textAlign: 'left',
//                                                 fontSize: 22,
//                                                 lineHeight: 25,
//                                                 color: '#fff',
//                                                 fontFamily: 'avenirbook'
//                                             }}
//                                                 adjustsFontSizeToFit>Habitudes</Text>
//                                         </View>
//                                         <View style={{
//                                             height: 1,
//                                             marginLeft: 30,
//                                             borderTopWidth: 2,
//                                             borderTopColor: '#fff',
//                                             width: width * 0.2,
//                                             marginBottom: 20
//                                         }} />
//                                         {props.item.repeated.map((item2, index2) => {
//                                             return <Bubble increaseTagIndex={props.increaseTagIndex} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef}
//                                                 update={() => props.updateBubble()}
//                                                 analytics={props.analytics} date={item2.date}
//                                                 key={index2}
//                                                 item={item2.bubble}
//                                                 deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)}
//                                                 dataBase={db} />;
//                                         })}
//                                     </View>
//                                 )}
//                             </View>
//                         )}
//                     />
//                 </>
//             );
//         } else {
//             return <View />;
//         }
//     }
// }