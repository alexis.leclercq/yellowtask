import React, { useState } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image, AsyncStorage, LayoutAnimation } from 'react-native';
import { View as ThemedView, TextInverted as ThemedTextInverted } from '../common/Themed';
import { height, width } from '../../constants/Layout';
import EditBubbleBox from '../EditBubbleBox';
import Bubble from "../Bubble";
import moment from "moment";
import 'moment/locale/fr';
import * as SQLite from 'expo-sqlite';
import { hasTrue } from '../../api/Lists';
import Separator from '../common/Separator';
import { translate } from '../../locales/i18n';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DraggableFlatList from 'react-native-draggable-dynamic-flatlist'

const db = SQLite.openDatabase('yellowtask.db');

export default function ListDay(props) {
    const [showMoreDone, setShowMoreDone] = useState(true);

    const data = [...Array(30)].map((d, index) => ({
        key: `item-${index}`,
        label: index,
        backgroundColor: `rgb(${Math.floor(Math.random() * 255)}, ${index * 5}, ${132})`,
    }))
    // async componentDidMount() {
    //     if (await AsyncStorage.getItem('YellowTaskShowMoreDone'))
    //         setState({ showMoreDone: true });
    //     else
    //         setState({ showMoreDone: false });
    // }

    // async componentDidUpdate(prevProps) {
    //     if (prevProps !== props) {
    //         if (await AsyncStorage.getItem('YellowTaskShowMoreDone'))
    //             setState({ showMoreDone: true });
    //         else
    //             setState({ showMoreDone: false });
    //     }
    // }

    const isToday = (date) => {
        return moment().locale('fr').format('L') === date;
    }

    const isTomorrow = (date) => {
        return moment().add("1", "days").locale('fr').format('L') === date;
    }

    const isNextTomorrow = (date) => {
        return moment().add("2", "days").locale('fr').format('L') === date;
    }

    const getFirstDate = (item, day) => {
        if (day) {
            if (isToday(item.date))
                return translate("today");
            else if (isTomorrow(item.date))
                return translate("tomorrow");
            else if (isNextTomorrow(item.date))
                return translate("afterTomorrow");
            else
                return day[0].toUpperCase() + day.slice(1);
        }
    }

    const getSecondDate = (item, day, month) => {
        if (item && item.date && month)
            if (isToday(item.date) || isTomorrow(item.date) || isNextTomorrow(item.date))
                return day[0].toUpperCase() + day.slice(1, 3) + " " + item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
            else
                return item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
    }

    const getAllArchived = (bubbles) => {
        let count = 0;
        let available = 0;
        for (let i in bubbles) {
            if (bubbles[i].archived === true && bubbles[i].date >= moment().locale('fr').subtract(1, "days").format('L'))
                count++;
            if (bubbles[i].date >= moment().locale('fr').subtract(1, "days").format('L'))
                available++;
        }
        if (count > 0)
            return count / available;
        else
            return 0;
    }

    const renderItemTest = ({ item, index, move, moveEnd, isActive }) => {
        if ((!item.repeat || !hasTrue(item.repeat)))
            if (((item.date >= moment().locale('fr').subtract(1, "days").format('L') || item.archived !== true)))
                if (!item.archived)
                    return (
                        <TouchableOpacity
                            style={{
                                height: 100,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                            onLongPress={move}
                            onPressOut={moveEnd}>
                            <Text style={{
                                fontWeight: 'bold',
                                color: 'white',
                                fontSize: 32,
                            }}>{index}</Text>
                        </TouchableOpacity>
                    )
        return <View />;
    }

    const renderItem = ({ item, index, move, moveEnd, isActive }) => {
        if ((!item.repeat || !hasTrue(item.repeat)))
            if (((item.date >= moment().locale('fr').subtract(1, "days").format('L') || item.archived !== true)))
                if (!item.archived)
                    return (
                        <Bubble onLongPress={move} onPressOut={moveEnd} increaseTagIndex={props.increaseTagIndex} key={index} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef} update={() => props.updateBubble()} analytics={props.analytics} item={item}
                            deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)} dataBase={db} />
                    )
        return <View />;
    }

    return (
        <View style={{ marginRight: props.carouselItems.length === 1 ? width * 0.12 : 0 }}>
            <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
                <View style={{ width: 'auto' }}>
                    <ThemedTextInverted style={{
                        textAlign: 'left',
                        fontSize: 22,
                        lineHeight: 25,
                        fontFamily: 'avenirbook'
                    }}
                        adjustsFontSizeToFit>{getFirstDate(props.item, props.day)}</ThemedTextInverted>
                </View>
                <View style={{ width: 'auto', alignItems: 'center', paddingLeft: 0 }}>
                    <ThemedTextInverted style={{
                        textAlign: 'left',
                        paddingLeft: 10,
                        fontSize: 14,
                        lineHeight: 17,
                        fontFamily: 'avenirbook'
                    }}
                        adjustsFontSizeToFit>{getSecondDate(props.item, props.day, props.month)}</ThemedTextInverted>
                </View>
            </View>
            <Separator />
            {console.log(props.carouselItems[props.index].bubbles)}
            <DraggableFlatList
                data={props.carouselItems[props.index].bubbles}
                renderItem={renderItem}
                keyExtractor={(item, index) => index}
                scrollPercent={5}
                onMoveEnd={({ data }) => console.log(data)}
            />
            {/* <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ height: height * 0.8 }}
            >
                {props.carouselItems && props.carouselItems[props.index] && props.carouselItems[props.index].bubbles && props.carouselItems[props.index].bubbles.length > 0 && (
                    <>
                        <ThemedTextInverted style={{ textAlign: 'right', fontSize: 12, lineHeight: 12, fontFamily: 'avenirbook', marginLeft: width * 0.8 }} adjustsFontSizeToFit>{Math.round(getAllArchived(props.carouselItems[props.index].bubbles) * 100) + "%"}</ThemedTextInverted>
                        <View style={{ height: 18 }}>
                            <ThemedView style={{ height: 5, marginBottom: 13, width: width * 0.8, marginLeft: 30, borderRadius: 20 }} />
                            <View style={{ position: 'absolute', backgroundColor: '#5BF7BA', height: 5, width: (width * 0.8) * getAllArchived(props.carouselItems[props.index].bubbles), marginLeft: 30, borderRadius: 20 }} />
                        </View>
                    </>
                )}
                {props.carouselItems && props.carouselItems[props.index] && props.carouselItems[props.index].bubbles && props.carouselItems[props.index].bubbles.length > 0 && props.carouselItems[props.index].bubbles.map((item, index) => {
                    if ((!item.repeat || !hasTrue(item.repeat)))
                        if (((item.date >= moment().locale('fr').subtract(1, "days").format('L') || item.archived !== true)))
                            if (!item.archived)
                                return (
                                    <Bubble increaseTagIndex={props.increaseTagIndex} key={index} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef} update={() => props.updateBubble()} analytics={props.analytics} item={item}
                                        deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)} dataBase={db} />
                                );
                })}
                <View>
                    <EditBubbleBox increaseTagIndex={props.increaseTagIndex} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} onAddBubble={(data) => props.addBubble(data, props.carouselItems[props.index].date)} />
                    {props.item && props.item.repeated && props.item.repeated.length > 0 && (
                        <View>
                            <View style={{
                                flexDirection: 'row',
                                paddingLeft: 30,
                                alignItems: 'center'
                            }}>
                                <Text style={{
                                    textAlign: 'left',
                                    fontSize: 22,
                                    lineHeight: 25,
                                    color: '#fff',
                                    fontFamily: 'avenirbook'
                                }}
                                    adjustsFontSizeToFit>Habitudes</Text>
                            </View>
                            <View style={{
                                height: 1,
                                marginLeft: 30,
                                borderTopWidth: 2,
                                borderTopColor: '#fff',
                                width: width * 0.2,
                                marginBottom: 20
                            }} />
                            {props.item.repeated.map((item2, index2) => {
                                return <Bubble increaseTagIndex={props.increaseTagIndex} key={index2} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef} update={() => props.updateBubble()} analytics={props.analytics} date={item2.date}
                                    item={item2.bubble}
                                    deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)} dataBase={db} />;
                            })}
                        </View>
                    )}
                    {props.carouselItems && props.carouselItems[props.index] && props.carouselItems[props.index].bubbles && props.carouselItems[props.index].bubbles.length > 0 && (
                        <View>
                            <TouchableOpacity onPress={async () => {
                                setShowMoreDone(!showMoreDone);
                                LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                                // if (await AsyncStorage.getItem('YellowTaskShowMoreDone'))
                                //     await AsyncStorage.removeItem('YellowTaskShowMoreDone')
                                // else
                                //     await AsyncStorage.setItem('YellowTaskShowMoreDone', "true");
                            }}>
                                <View style={{
                                    flexDirection: 'row',
                                    paddingLeft: 30,
                                    alignItems: 'center',
                                }}>
                                    <ThemedTextInverted style={{
                                        textAlign: 'left',
                                        fontSize: 22,
                                        lineHeight: 25,
                                        fontFamily: 'avenirbook'
                                    }}
                                        adjustsFontSizeToFit>{translate('congrats')}  </ThemedTextInverted>
                                    <Image source={showMoreDone ? require('../../assets/bubble_up.png') : require('../../assets/bubble_down.png')} resizeMode="contain" style={{ height: 12, width: 12, alignSelf: 'center' }} />
                                </View>
                                <Separator />
                            </TouchableOpacity>
                            {props.carouselItems && props.carouselItems[props.index] && props.carouselItems[props.index].bubbles.length > 0 && showMoreDone && props.carouselItems[props.index].bubbles.map((item2, index2) => {
                                if (((item2.date >= moment().locale('fr').subtract(1, "days").format('L') || item2.archived !== true)))
                                    if (item2.archived)
                                        return (
                                            <Bubble increaseTagIndex={props.increaseTagIndex} key={index2} addTag={props.addTag} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} confettiRef={props.confettiRef} update={() => props.updateBubble()} analytics={props.analytics} item={item2}
                                                deleteBubble={(bubbleRef) => props.removeBubble(bubbleRef)} dataBase={db} />
                                        );
                            })}
                        </View>
                    )}
                </View>
            </KeyboardAwareScrollView> */}
        </View>
    );
}
