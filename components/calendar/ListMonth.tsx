import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Image, Text, FlatList, ScrollView } from 'react-native';
import { height, width } from '../../constants/Layout';
import EditBubbleBox from '../EditBubbleBox';
import Bubble from "../Bubble";
import moment from "moment";
import 'moment/locale/fr';
import * as SQLite from 'expo-sqlite';
import { hasTrue } from '../../api/Lists';
import Separator from '../common/Separator';
import BubbleWeek from './BubbleWeek';
import { translate, locale } from '../../locales/i18n';
import { TextInverted as ThemedTextInverted } from '../common/Themed';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const db = SQLite.openDatabase('yellowtask.db');

export default function ListMonth(props) {

    const isThisMonth = (date) => {
        return moment(date, 'DD/MM/YYYY', true).month() === moment().month();
    }

    const isNextMonth = (date) => {
        return moment(date, 'DD/MM/YYYY', true).month() === moment().month() + 1;
    }

    const getFirstDateMonth = (item) => {
        if (isThisMonth(item.date_start))
            return translate("myMonth");
        else if (isNextMonth(item.date_start))
            return translate("nextMonth");
        else
            return translate("month");
    }

    const getSecondDateMonth = (item, day, month) => {
        if (props.item && props.item.date_end) {
            let end_date = moment(new Date(props.item.date_end[3] + props.item.date_end[4] + "/" + props.item.date_end.slice(0, 3) + props.item.date_end.slice(6))).locale('fr').subtract(1, 'days').format('L');
            let end_month = moment(new Date(props.item.date_end[3] + props.item.date_end[4] + "/" + props.item.date_end.slice(0, 3) + props.item.date_end.slice(6))).locale('fr').subtract(1, 'days').format('MMM');
            if (item && item.date_start)
                return translate('from') + " " + item.date_start.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3) + " " + translate('to') + " " + end_date.slice(0, 2) + " " + end_month[0].toUpperCase() + end_month.slice(1, 3);
        }
    }

    const isToday = (date) => {
        return moment().locale('fr').format('L') === date;
    }

    const isTomorrow = (date) => {
        return moment().add("1", "days").locale('fr').format('L') === date;
    }

    const isNextTomorrow = (date) => {
        return moment().add("2", "days").locale('fr').format('L') === date;
    }

    const getFirstDate = (item, day) => {
        if (day) {
            if (isToday(item.date))
                return translate("today");
            else if (isTomorrow(item.date))
                return translate("tomorrow");
            else if (isNextTomorrow(item.date))
                return translate("afterTomorrow");
            else
                return day[0].toUpperCase() + day.slice(1);
        }
    }

    const getSecondDate = (item, day, month) => {
        if (item && item.date && month)
            if (isToday(item.date) || isTomorrow(item.date) || isNextTomorrow(item.date))
                return day[0].toUpperCase() + day.slice(1, 3) + " " + item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
            else
                return item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
    }

    let monthDisplay = translate(moment(props.item.date_start, 'DD/MM/YYYY', true).locale('en').format('MMMM').toLowerCase());
    
    return (
        <View style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
                <View style={{ width: 'auto' }}>
                    <ThemedTextInverted style={{
                        textAlign: 'left',
                        fontSize: 22,
                        lineHeight: 25,
                        fontFamily: 'avenirbook'
                    }}
                        adjustsFontSizeToFit>{monthDisplay[0].toUpperCase() + monthDisplay.substr(1)}</ThemedTextInverted>
                </View>
            </View>
            <Separator />
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ marginRight: props.carouselItems.length === 1 ? width * 0.12 : 0 }}>
                <BubbleWeek bubbleIndex={0} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={1} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={2} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={3} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={4} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={5} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={6} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={7} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={8} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={9} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={10} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={11} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={12} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={13} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={14} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={15} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={16} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={17} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={18} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={19} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={20} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={21} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={22} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={23} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={24} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={25} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={26} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={27} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={28} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={29} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={30} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
            </KeyboardAwareScrollView>
        </View>
    );
}