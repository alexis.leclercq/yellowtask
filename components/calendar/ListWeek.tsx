import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity, Image, Text, FlatList, ScrollView } from 'react-native';
import { height, width } from '../../constants/Layout';
import EditBubbleBox from '../EditBubbleBox';
import Bubble from "../Bubble";
import moment from "moment";
import * as SQLite from 'expo-sqlite';
import { hasTrue } from '../../api/Lists';
import Separator from '../common/Separator';
import { TextInverted as ThemedTextInverted } from '../common/Themed';
import BubbleWeek from './BubbleWeek';
import { translate } from '../../locales/i18n';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const db = SQLite.openDatabase('yellowtask.db');

export default function ListWeek(props) {

    const isThisWeek = (date) => {
        return moment(date, 'DD/MM/YYYY', true).week() === moment().week();
    }

    const isNextWeek = (date) => {
        return moment(date, 'DD/MM/YYYY', true).week() === moment().week() + 1;
    }

    const getFirstDateWeek = (item) => {
        if (isThisWeek(item.date_start))
            return translate("myWeek");
        else if (isNextWeek(item.date_start))
            return translate("nextWeek");
        else
            return translate("week");
    }

    const getSecondDateWeek = (item, day, month) => {
        if (props.item && props.item.date_end) {
            let end_date = moment(new Date(props.item.date_end[3] + props.item.date_end[4] + "/" + props.item.date_end.slice(0, 3) + props.item.date_end.slice(6))).locale('fr').subtract(1, 'days').format('L');
            let end_month = moment(new Date(props.item.date_end[3] + props.item.date_end[4] + "/" + props.item.date_end.slice(0, 3) + props.item.date_end.slice(6))).locale('fr').subtract(1, 'days').format('MMM');
            if (item && item.date_start)
                return translate('from') + " " + item.date_start.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3) + " " + translate('to') + " " + end_date.slice(0, 2) + " " + end_month[0].toUpperCase() + end_month.slice(1, 3);
        }
    }

    const isToday = (date) => {
        return moment().locale('fr').format('L') === date;
    }

    const isTomorrow = (date) => {
        return moment().add("1", "days").locale('fr').format('L') === date;
    }

    const isNextTomorrow = (date) => {
        return moment().add("2", "days").locale('fr').format('L') === date;
    }

    const getFirstDate = (item, day) => {
        if (day) {
            if (isToday(item.date))
                return translate("today");
            else if (isTomorrow(item.date))
                return translate("tomorrow");
            else if (isNextTomorrow(item.date))
                return translate("afterTomorrow");
            else
                return day[0].toUpperCase() + day.slice(1);
        }
    }

    const getSecondDate = (item, day, month) => {
        if (item && item.date && month)
            if (isToday(item.date) || isTomorrow(item.date) || isNextTomorrow(item.date))
                return day[0].toUpperCase() + day.slice(1, 3) + " " + item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
            else
                return item.date.slice(0, 2) + " " + month[0].toUpperCase() + month.slice(1, 3);
    }

    return (
        <View style={{ flexDirection: 'column' }}>
            <View style={{ flexDirection: 'row', paddingLeft: 30, alignItems: 'center' }}>
                <View style={{ width: 'auto' }}>
                    <ThemedTextInverted style={{
                        textAlign: 'left',
                        fontSize: 22,
                        lineHeight: 25,
                        fontFamily: 'avenirbook'
                    }}
                        adjustsFontSizeToFit>{getFirstDateWeek(props.item)}</ThemedTextInverted>
                </View>
                <View style={{ width: 'auto', alignItems: 'center', paddingLeft: 0 }}>
                    <ThemedTextInverted style={{
                        textAlign: 'left',
                        paddingLeft: 10,
                        fontSize: 14,
                        lineHeight: 17,
                        fontFamily: 'avenirbook'
                    }}
                        adjustsFontSizeToFit>{getSecondDateWeek(props.item, props.day, props.month)}</ThemedTextInverted>
                </View>
            </View>
            <Separator />
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{ marginRight: props.carouselItems.length === 1 ? width * 0.12 : 0 }}>
                <BubbleWeek bubbleIndex={0} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={1} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={2} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={3} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={4} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={5} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
                <BubbleWeek bubbleIndex={6} addTag={props.addTag} increaseTagIndex={props.increaseTagIndex} removeTag={props.removeTag} editTag={props.editTag} tags={props.tags} carouselItems={props.carouselItems} index={props.index} confettiRef={props.confettiRef} updateBubble={props.updateBubble} removeBubble={props.removeBubble} analytics={props.analytics} addBubble={props.addBubble} />
            </KeyboardAwareScrollView>
        </View>
    );
}