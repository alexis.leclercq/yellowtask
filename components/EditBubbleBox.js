import React from 'react';
import { StyleSheet, FlatList, Image, LayoutAnimation, Platform, AsyncStorage, ScrollView, View, TouchableOpacity as DefaultTouchableOpacity } from 'react-native';
import { View as ThemedView, TouchableOpacity, Text, TextInput, ViewInverted } from '../components/common/Themed';
import { BlurView } from 'expo-blur';
import { width } from '../constants/Layout';
import CheckBox from "react-native-check-box";
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from "moment";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import * as ExpoHaptics from 'expo-haptics';
import { translate } from '../locales/i18n';
import { Overlay } from "react-native-elements";
import ThreeDots from './SVG/ThreeDots';
import HashTag from './SVG/HashTag';
import Reminder from './SVG/Reminder';
import UnderNotArchived from './SVG/UnderNotArchived';

async function getiOSNotificationPermission() {
    const { status } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    if (status !== 'granted') {
        await Permissions.askAsync(Permissions.NOTIFICATIONS);
    }
}

export default class EditBubbleBox extends React.Component {

    constructor(props) {
        super(props);

        this._isMounted = false;
    }

    state = {
        name: null,
        underTask: [],
        note: null,
        tag: [],
        repeat: null,
        reminders: [],
        ref: null,
        showMoreDrop: false,
        showMoreTag: false,
        showMoreReminder: false,
        dateActivate: false,
        date: new Date(),
        startingIndex: 0,
        addTag: false,
        newTag: null,
        newColor: null,
        newId: null,
        openTag: null,
        tagIndex: null,
        removeTag: false,
        openTagPosition: {
            width: null,
            height: null,
            x: null,
            y: null,
        }
    };

    async componentDidMount() {
        this._isMounted = true;
        if (this._isMounted && await AsyncStorage.getItem('YellowTaskShowMoreTag'))
            this._isMounted && this.setState({ showMoreTag: true });
        if (this._isMounted && await AsyncStorage.getItem('YellowTaskShowMoreReminder'))
            this._isMounted && this.setState({ showMoreReminder: true });
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    hasTrue() {
        for (let i in this.state.repeat) {
            if (this.state.repeat[i] === true)
                return true;
        }
        return false;
    }

    addNewUnderTask(e) {
        const { underTask } = this.state;

        if (e.nativeEvent.text.length == 0)
            return;
        underTask.push({ id: underTask.length, name: e.nativeEvent.text, archived: false });
        if (this._isMounted)
            this.setState({ underTask });
    }

    addTag(name, color) {
        const { tag } = this.state;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy)
        let check = -1;
        for (let i in tag) {
            if (tag[i].name === name)
                check = i;
        }
        if (check >= 0)
            tag.splice(check, 1);
        else
            tag.push({ name, color });
        if (this._isMounted)
            this.setState({ tag });
    }

    addTag(index) {
        const { tag } = this.state;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy)
        let check;
        let newTag = tag;
        if (newTag == null) {
            newTag = [];
            check = -1;
        } else
            check = newTag.indexOf(index);
        if (check !== -1)
            newTag.splice(check, 1);
        else
            newTag.push(index);
        this._isMounted && this.setState({ item: { ...this.state.item, tag: newTag } });
    }

    hasTrueItem(repeat) {
        for (let i in repeat) {
            if (repeat[i] === true)
                return true;
        }
        return false;
    }

    getLastDay(repeat) {
        let { archived } = this.state;

        if (this.hasTrueItem(repeat)) {
            if (!archived || !archived.length)
                archived = [];
            for (let i = 0; i != 7; i++)
                archived.push(moment(new Date()).subtract(i, 'days').format('L'));
        }
        if (this._isMounted)
            this.setState({ archived });
    }

    addRepeat(key, state) {
        let { repeat } = this.state;
        let check = false;

        if (!repeat)
            repeat = {
                monday: false,
                tuesday: false,
                wednesday: false,
                thursday: false,
                friday: false,
                saturday: false,
                sunday: false
            };
        else {
            check = true;
        }
        repeat[key] = state;
        if (check === true)
            this.getLastDay(repeat);
        if (this._isMounted)
            this.setState({ repeat });
    }

    addBubble() {
        const { name, underTask, note, tag, repeat, reminders } = this.state;
        if (!name)
            return;
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.props.onAddBubble({ name, underTask: JSON.stringify(underTask), note, tag: JSON.stringify(tag), repeat: JSON.stringify(repeat), reminders: JSON.stringify(reminders) });
        if (this._isMounted)
            this.setState({ name: null, underTask: [], note: null, tag: [], reminders: [], repeat: null });
        this.close();
    }

    changeLayoutDrop = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this._isMounted)
            this.setState({ showMoreDrop: !this.state.showMoreDrop });
    }

    listenForNotifications = () => {
        Notifications.addListener(notification => {
            if (notification.origin === 'received' && Platform.OS === 'ios') {
                //Alert.alert(notification.origin);
            }
        });
    };

    async sendNotif() {
        const localnotification = {
            title: 'YellowTask',
            body: this.state.name,
            android: {
                sound: true,
            },
            ios: {
                sound: true,
            },
        };
        let date = new Date(this.state.date);

        await getiOSNotificationPermission();
        this.listenForNotifications();
        const schedulingOptions = { time: date };
        let id = Notifications.scheduleLocalNotificationAsync(
            localnotification,
            schedulingOptions
        );
    };

    addReminder() {
        let { reminders } = this.state;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (!reminders)
            reminders = [];
        reminders.push(this.state.date.toUTCString());
        if (this._isMounted)
            this.setState({ reminders });
        this.sendNotif();
    }

    removeReminder(key) {
        let { reminders } = this.state;

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        reminders.splice(key, 1);
        if (this._isMounted)
            this.setState({ reminders });
    }

    open() {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this._isMounted)
            this.setState({ addBubble: true });
    }

    close() {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this._isMounted)
            this.setState({ addBubble: false });
    }

    hasMultiTags() {
        for (let i in this.props.tags) {
            if (hasTag(this.props.tags[i].id, this.state.tag))
                return true;
        }
        return false;
    }

    getTagInfoById(id) {
        for (let i in this.props.tags) {
            if (this.props.tags[i].id === id)
                return this.props.tags[i];
        }
        return null;
    }

    render() {
        const { repeat, tag, underTask, name, note } = this.state;

        return (
            <>
                {!this.state.addBubble ? (
                    <TouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => this.open()}
                        style={{
                            width: width * 0.8,
                            marginLeft: 30,
                            height: 50,
                            alignItems: 'center',
                            borderRadius: 50,
                            borderColor: '#dcdcdc',
                            marginBottom: 20,
                            flexDirection: 'row'
                        }}>

                            <Text style={{
                                width: (width * 0.8),
                                textAlign: 'left',
                                fontSize: 17,
                                lineHeight: 20,
                                fontFamily: 'avenirbook',
                                paddingRight: 10,
                                paddingLeft: 20,
                                paddingTop: 4
                            }}>
                                {translate("addTask")}
                            </Text>
                    </TouchableOpacity>
                ) : (
                        <>
                            <ThemedView style={styles.container}>
                                <View style={styles.blurView}>
                                    {tag && tag.length > 0 && this.hasMultiTags() && (
                                        <View style={{
                                            paddingTop: 0,
                                        }}>
                                            <View style={{ paddingTop: 15, paddingBottom: 0, paddingHorizontal: 10, flexDirection: 'row', flexWrap: "wrap" }}>
                                                {tag.map((data, index) => {
                                                    let tagData = this.getTagInfoById(data);
                                                    if (tagData) {
                                                        return (
                                                            <Tag key={index} color={tagData.color} text={tagData.name.toUpperCase()} />
                                                        );
                                                    }
                                                    return null;
                                                })}
                                            </View>
                                        </View>
                                    )}
                                    <View style={styles.row}>
                                        <TextInput keyboardType={Platform.OS === "ios" ? "web-search" : "default"} autoFocus onSubmitEditing={() => this.addBubble()} placeholder={translate("addTask")} onChangeText={(e) => this.setState({ name: e })} style={styles.taskInput} value={name} />
                                        <View style={styles.cancelView}>
                                            <Text onPress={() => this.close()} style={styles.cancel}>{translate("cancel")}</Text>
                                        </View>
                                    </View>
                                    <Separator />
                                    <View>
                                        <FlatList listKey={0} data={underTask} keyExtractor={item => item.id.toString()} ListFooterComponent={() => {
                                            return (
                                                <View style={styles.underTaskView}>
                                                    <TextInput placeholder={translate("addSubTask")} onEndEditing={(e) => this.addNewUnderTask(e)} style={styles.underTaskInput} />
                                                </View>
                                            );
                                        }} renderItem={({ item }) => {
                                            return (
                                                <View style={styles.underTaskView}>
                                                    {/* <Image source={require('../assets/not_archived.png')} style={styles.archivedImg} resizeMode={"contain"} /> */}
                                                    <UnderNotArchived color={"#000"} />
                                                    <Text style={[styles.underTaskText, styles.paddingLeft10]}>{item.name}</Text>
                                                </View>
                                            );
                                        }} />
                                    </View>
                                    <Separator />
                                    <View style={styles.row}>
                                        <TextInput multiline placeholder={translate("addNote")} onChangeText={(e) => this.setState({ note: e })} style={styles.noteInput} value={note} />
                                        {String(name).length > 0 && (
                                            <DefaultTouchableOpacity activeOpacity={100} onPress={() => this.addBubble()} style={styles.okView}>
                                                <Text style={styles.ok}>OK</Text>
                                            </DefaultTouchableOpacity>
                                        )}
                                    </View>
                                    <Separator />
                                    <DefaultTouchableOpacity style={{ marginLeft: 20, marginTop: 10, marginBottom: this.state.showMoreTag ? 0 : 10 }} activeOpacity={100} onPress={async () => {
                                        if (this._isMounted)
                                            this.setState({ showMoreTag: !this.state.showMoreTag });
                                        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                                        if (await AsyncStorage.getItem('YellowTaskShowMoreTag'))
                                            await AsyncStorage.removeItem('YellowTaskShowMoreTag')
                                        else
                                            await AsyncStorage.setItem('YellowTaskShowMoreTag', "true");
                                    }}>
                                        <Text style={{ fontSize: 17 }}><HashTag color={"#000"} />  ID  <Image source={this.state.showMoreTag ? require('../assets/bubble_up.png') : require('../assets/bubble_down.png')} resizeMode="contain" style={{ height: 12, width: 12, alignSelf: 'center' }} /></Text>
                                    </DefaultTouchableOpacity>
                                    {this.state.showMoreTag && (
                                        <View style={[styles.bigTagView, { padding: 10, flexDirection: 'row', flexWrap: "wrap" }]}>
                                            {this.props.tags.map((data, index) => {
                                                return (
                                                    <TagAdd key={index} index={index} remove={() => this.setState({ removeTag: true, tagIndex: index })} setColor={(color) => { this.setState({ newColor: color }); this.props.editTag(this.state.newTag, color, this.state.newId, index); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} openTag={this.state.openTag} onLayout={(event) => this.setState({ openTagPosition: event.nativeEvent.layout })} onLongPress={() => { this.setState({ openTag: index, newTag: data.name, newColor: data.color, newId: data.id }); ExpoHaptics.impactAsync(ExpoHaptics.ImpactFeedbackStyle.Heavy) }} onPress={() => this.addTag(this.state.openTag === index ? this.state.newId : data.id)} id={this.state.openTag === index ? this.state.newId : data.id} color={this.state.openTag === index ? this.state.newColor : data.color} text={this.state.openTag === index ? this.state.newTag : data.name} tag={tag} onEndEditing={(e) => { this.props.editTag(this.state.newTag, this.state.newColor, this.state.newId, index); this.setState({ addTag: false }); }} onChangeText={(text) => this.setState({ newTag: text })} />
                                                );
                                            })}
                                            {this.state.addTag ? (
                                                <View style={{ backgroundColor: '#4E4E4E', borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                                                    <TextInput autoFocus adjustsFontSizeToFit onEndEditing={(e) => { this.props.addTag(this.state.newTag, '#4E4E4E'); this.setState({ addTag: false, newTag: null }); }} onChangeText={(text) => this.setState({ newTag: text })} value={this.state.newTag} style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: 'avenirbook', marginTop: 5 }} />
                                                </View>
                                            ) : (
                                                    <DefaultTouchableOpacity activeOpacity={100} onPress={() => this.setState({ addTag: true, newColor: null, newTag: null, newId: null, openTag: null })} style={{ height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                                                        <Text style={{ fontSize: 26 }}>+</Text>
                                                    </DefaultTouchableOpacity>
                                                )}
                                            {this.state.openTag !== null && this.state.openTag !== undefined && this.state.openTag >= 0 && (
                                                <ViewInverted style={{ position: 'absolute', left: 10, top: this.state.openTagPosition.y - this.state.openTagPosition.height - 9, height: 40, width: 280, borderRadius: 25, flexDirection: 'row', alignItems: 'center' }}>
                                                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#F35B5B' }); this.props.editTag(this.state.newTag, '#F35B5B', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#F35B5B', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#FFBE26' }); this.props.editTag(this.state.newTag, '#FFBE26', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#FFBE26', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#16B476' }); this.props.editTag(this.state.newTag, '#16B476', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#16B476', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#0094FF' }); this.props.editTag(this.state.newTag, '#0094FF', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#0094FF', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#B652E5' }); this.props.editTag(this.state.newTag, '#B652E5', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#B652E5', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                        <TouchableOpacity onPress={() => { this.setState({ newColor: '#4E4E4E' }); this.props.editTag(this.state.newTag, '#4E4E4E', this.state.newId, this.state.openTag); this.setState({ addTag: false, newTag: null, newColor: null, newId: null, openTag: null }); }} style={{ backgroundColor: '#4E4E4E', borderRadius: 50, width: 30, height: 30, marginHorizontal: 5 }} />
                                                    </ScrollView>
                                                    <TouchableOpacity onPress={() => this.setState({ removeTag: true, tagIndex: this.state.openTag })} style={{ width: 30, height: 30, marginHorizontal: 5 }}>
                                                        <Image source={require('../assets/blackbin2.png')} resizeMode={"contain"} style={{ width: 30, height: 30 }} />
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.setState({ openTag: null, newTag: null, newColor: null, newId: null, addTag: false })} style={{ width: 30, height: 30, marginHorizontal: 5, paddingRight: 5, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={require('../assets/removetag.png')} resizeMode={"contain"} style={{ width: 15, height: 15 }} />
                                                    </TouchableOpacity>
                                                </ViewInverted>
                                            )}
                                        </View>
                                    )}
                                    {!this.state.showMoreDrop && (<TouchableOpacity activeOpacity={100} onPress={() => this.changeLayoutDrop()} style={{ alignItems: 'center', justifyContent: 'center', height: 50, width: '100%', borderRadius: 25 }}>
                                        {/* <Image source={require('../assets/3hdots.png')} style={{ height: 25, width: 25, marginTop: 5 }} resizeMode={"contain"} /> */}
                                        <View style={{ height: 25, width: 25, marginTop: 5 }}>
                                            <ThreeDots color={"#000"} />
                                        </View>
                                    </TouchableOpacity>)}
                                    {this.state.showMoreDrop && (
                                        <DefaultTouchableOpacity activeOpacity={100} onLongPress={() => null} onPress={() => null}>
                                            <Separator />
                                            <DefaultTouchableOpacity style={{ marginLeft: 20, marginTop: 10, marginBottom: this.state.showMoreReminder ? 0 : 10 }} activeOpacity={100} onPress={async () => {
                                                if (this._isMounted)
                                                    this.setState({ showMoreReminder: !this.state.showMoreReminder });
                                                LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
                                                if (await AsyncStorage.getItem('YellowTaskShowMoreReminder'))
                                                    await AsyncStorage.removeItem('YellowTaskShowMoreReminder')
                                                else
                                                    await AsyncStorage.setItem('YellowTaskShowMoreReminder', "true");
                                            }}>
                                                <Text style={{ fontSize: 17 }}><Reminder color={"#000"} />  {translate('reminder')}  <Image source={this.state.showMoreReminder ? require('../assets/bubble_up.png') : require('../assets/bubble_down.png')} resizeMode="contain" style={{ height: 12, width: 12, alignSelf: 'center' }} /></Text>
                                            </DefaultTouchableOpacity>
                                            {/* <Separator />
                                            <View style={{ flexDirection: 'row' }}>
                                                <Text style={{ fontFamily: 'avenirbook', fontSize: 14, color: '#000', marginVertical: 5, paddingTop: 10, width: '90%' }}>Habitude :</Text>
                                            </View>
                                            <View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <View style={{ flex: 1 }}>
                                                        <TouchableOpacity onPress={() => {
                                                            this.addRepeat('monday', repeat ? !repeat.monday : true);
                                                        }}>
                                                            <CheckBox
                                                                checkBoxColor='#000'
                                                                checkedCheckBoxColor='#FECE2F'
                                                                style={{ paddingVertical: 10 }}
                                                                onClick={() => {
                                                                    this.addRepeat('monday', repeat ? !repeat.monday : true);
                                                                }}
                                                                isChecked={repeat ? repeat.monday : false}
                                                                rightText={"Lundi"}
                                                                rightTextStyle={{
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook',
                                                                    fontSize: 18,
                                                                    marginTop: 5
                                                                }}
                                                            />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={() => {
                                                            this.addRepeat('tuesday', repeat ? !repeat.tuesday : true);
                                                        }}>
                                                            <CheckBox
                                                                checkBoxColor='#000'
                                                                checkedCheckBoxColor='#FECE2F'
                                                                style={{ paddingVertical: 10 }}
                                                                onClick={() => {
                                                                    this.addRepeat('tuesday', repeat ? !repeat.tuesday : true);
                                                                }}
                                                                isChecked={repeat ? repeat.tuesday : false}
                                                                rightText={"Mardi"}
                                                                rightTextStyle={{
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook',
                                                                    fontSize: 18,
                                                                    marginTop: 5
                                                                }}
                                                            />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={() => {
                                                            this.addRepeat('wednesday', repeat ? !repeat.wednesday : true);
                                                        }}>
                                                            <CheckBox
                                                                checkBoxColor='#000'
                                                                checkedCheckBoxColor='#FECE2F'
                                                                style={{ paddingVertical: 10 }}
                                                                onClick={() => {
                                                                    this.addRepeat('wednesday', repeat ? !repeat.wednesday : true);
                                                                }}
                                                                isChecked={repeat ? repeat.wednesday : false}
                                                                rightText={"Mercredi"}
                                                                rightTextStyle={{
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook',
                                                                    fontSize: 18,
                                                                    marginTop: 5
                                                                }}
                                                            />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={() => {
                                                            this.addRepeat('thursday', repeat ? !repeat.thursday : true);
                                                        }}>
                                                            <CheckBox
                                                                checkBoxColor='#000'
                                                                checkedCheckBoxColor='#FECE2F'
                                                                style={{ paddingVertical: 10 }}
                                                                onClick={() => {
                                                                    this.addRepeat('thursday', repeat ? !repeat.thursday : true);
                                                                }}
                                                                isChecked={repeat ? repeat.thursday : false}
                                                                rightText={"Jeudi"}
                                                                rightTextStyle={{
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook',
                                                                    fontSize: 18,
                                                                    marginTop: 5
                                                                }}
                                                            />
                                                        </TouchableOpacity>
                                                    </View>
                                                    <View style={{ flex: 1 }}>
                                                        <TouchableOpacity onPress={() => {
                                                            this.addRepeat('friday', repeat ? !repeat.friday : true);
                                                        }}>
                                                            <CheckBox
                                                                checkBoxColor='#000'
                                                                checkedCheckBoxColor='#FECE2F'
                                                                style={{ paddingVertical: 10 }}
                                                                onClick={() => {
                                                                    this.addRepeat('friday', repeat ? !repeat.friday : true);
                                                                }}
                                                                isChecked={repeat ? repeat.friday : false}
                                                                rightText={"Vendredi"}
                                                                rightTextStyle={{
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook',
                                                                    fontSize: 18,
                                                                    marginTop: 5
                                                                }}
                                                            />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={() => {
                                                            this.addRepeat('saturday', repeat ? !repeat.saturday : true);
                                                        }}>
                                                            <CheckBox
                                                                checkBoxColor='#000'
                                                                checkedCheckBoxColor='#FECE2F'
                                                                style={{ paddingVertical: 10 }}
                                                                onClick={() => {
                                                                    this.addRepeat('saturday', repeat ? !repeat.saturday : true);
                                                                }}
                                                                isChecked={repeat ? repeat.saturday : false}
                                                                rightText={"Samedi"}
                                                                rightTextStyle={{
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook',
                                                                    fontSize: 18,
                                                                    marginTop: 5
                                                                }}
                                                            />
                                                        </TouchableOpacity>
                                                        <TouchableOpacity onPress={() => {
                                                            this.addRepeat('sunday', repeat ? !repeat.sunday : true);
                                                        }}>
                                                            <CheckBox
                                                                checkBoxColor='#000'
                                                                checkedCheckBoxColor='#FECE2F'
                                                                style={{ paddingVertical: 10 }}
                                                                onClick={() => {
                                                                    this.addRepeat('sunday', repeat ? !repeat.sunday : true);
                                                                }}
                                                                isChecked={repeat ? repeat.sunday : false}
                                                                rightText={"Dimanche"}
                                                                rightTextStyle={{
                                                                    color: '#000',
                                                                    fontFamily: 'avenirbook',
                                                                    fontSize: 18,
                                                                    marginTop: 5
                                                                }}
                                                            />
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </View> */}
                                            {/* <Separator /> */}
                                            {this.state.showMoreReminder && <View style={{ marginHorizontal: 20 }}>
                                                <View style={{ flexDirection: 'row' }}>
                                                    {Platform.OS === 'ios' ? <DateTimePicker
                                                        testID="dateTimePicker"
                                                        mode={'datetime'}
                                                        locale={'fr'}
                                                        is24Hour={true}
                                                        display="default"
                                                        onChange={(event, date) => this.setState({ date, dateActivate: false })}
                                                        value={this.state.date}
                                                        style={{ height: 50, width: (width * 0.6) - 10, marginLeft: 10 }}
                                                    /> : this.state.dateActivate ?
                                                            (
                                                                <>
                                                                    <DateTimePicker
                                                                        testID="dateTimePicker"
                                                                        mode={'datetime'}
                                                                        locale={'fr'}
                                                                        is24Hour={true}
                                                                        display="default"
                                                                        onChange={(event, date) => this.setState({ date, dateActivate: false })}
                                                                        value={this.state.date}
                                                                        style={{ width: (width * 0.6) - 10, marginLeft: 10 }}
                                                                    />
                                                                </>)
                                                            : <Button title="Sélectionner une date" style={{ width: (width * 0.6) - 10, marginLeft: 10 }} onPress={() => this.setState({ dateActivate: true })} />}
                                                </View>
                                                <DefaultTouchableOpacity activeOpacity={100} onPress={() => this.addReminder()} style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 5 }}>
                                                    <Text numberOfLines={1} style={{
                                                        textAlign: 'center',
                                                        fontSize: 18,
                                                        fontFamily: 'avenirblack'
                                                    }} adjustsFontSizeToFit>+</Text>
                                                </DefaultTouchableOpacity>
                                                {this.state.reminders && this.state.reminders.length > 0 && (
                                                    this.state.reminders.map((data, index) => (
                                                        <View key={index} style={{
                                                            height: 50,
                                                            justifyContent: 'center',
                                                            flexDirection: 'row',
                                                        }}>
                                                            <Text numberOfLines={1} style={{
                                                                textAlign: 'center',
                                                                marginTop: 7,
                                                                fontSize: 20,
                                                                lineHeight: 23,
                                                                fontFamily: 'avenirbook'
                                                            }}
                                                                adjustsFontSizeToFit>{moment(new Date(data)).format('lll')}</Text>
                                                            <DefaultTouchableOpacity activeOpacity={100} onPress={() => this.removeReminder(index)} style={{ height: 30, width: 30, paddingTop: 5 }}><Image
                                                                source={require('../assets/blackbin.png')} resizeMode={"contain"}
                                                                style={{ height: 20, width: 20, marginLeft: 20 }} /></DefaultTouchableOpacity>
                                                        </View>
                                                    ))
                                                )}
                                            </View>}
                                        </DefaultTouchableOpacity>
                                    )}
                                </View>
                            </ThemedView>
                            {
                                this.state.removeTag && (
                                    <Overlay isVisible={true} onBackdropPress={() => this._isMounted && this.setState({ removeTag: false, openTag: null, newTag: null, newColor: null, newId: null, addTag: false })}
                                        overlayBackgroundColor={'rgba(0, 0, 0, 0)'} overlayStyle={{
                                            width: width * 0.8,
                                            height: 160,
                                            backgroundColor: '#FFF',
                                            bottom: '5%',
                                            borderRadius: 15,
                                            opacity: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginTop: 10
                                        }}>
                                        <>
                                            <Text style={{
                                                lineHeight: 20,
                                                paddingHorizontal: 5,
                                                textAlign: 'left',
                                                fontSize: 18,
                                                lineHeight: 21,
                                                color: '#C4C4C4',
                                                fontFamily: 'avenirbook',
                                                marginTop: 5,
                                            }}>{translate("removeTag")}</Text>
                                            <TouchableOpacity onPress={() => { this.props.removeTag(this.state.tagIndex); this.setState({ removeTag: false, openTag: null, newTag: null, newColor: null, newId: null, addTag: false }) }} style={{ backgroundColor: '#F35B5B', borderRadius: 10, height: 45, width: '100%', marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={{ textAlign: 'center', fontSize: 17, lineHeight: 20, color: '#FFFFFF', fontFamily: 'avenirbook' }}>{translate("delete")}</Text>
                                            </TouchableOpacity>
                                        </>
                                    </Overlay>
                                )
                            }
                        </>
                    )}
            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: width * 0.8,
        marginLeft: 30,
        alignItems: 'center',
        borderRadius: 25,
        marginBottom: 20,
    },
    blurView: {
        width: width * 0.8,
        borderRadius: 25,
    },
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 7,
    },
    cancel: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        lineHeight: 20,
        fontFamily: 'avenirbook',
    },
    cancelView: {
        minHeight: 45,
        width: '40%',
        justifyContent: 'center',
    },
    ok: {
        textAlign: 'right',
        marginRight: 20,
        fontSize: 17,
        lineHeight: 20,
        fontFamily: 'avenirblack',
        color: '#FECE2F'
    },
    okView: {
        width: '20%',
        minHeight: 45,
        justifyContent: 'center',
    },
    taskInput: {
        minHeight: 45,
        paddingLeft: 20,
        width: '60%',
        textAlign: 'left',
        fontSize: 17,
        lineHeight: 20,
        fontFamily: 'avenirbook',
    },
    underTaskInput: {
        minHeight: 45,
        paddingTop: 3,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        lineHeight: 18,
        fontFamily: 'avenirbook',
    },
    underTaskText: {
        minHeight: 45,
        paddingTop: 16,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        lineHeight: 18,
        fontFamily: 'avenirbook',
    },
    noteInput: {
        minHeight: 45,
        paddingTop: 14,
        paddingLeft: 20,
        width: '80%',
        textAlign: 'left',
        fontSize: 15,
        lineHeight: 18,
        fontFamily: 'avenirbook',
    },
    row: {
        flexDirection: 'row',
    },
    tagView: {
        marginHorizontal: 20,
        alignItems: 'flex-start'
    },
    bigTagView: {
        paddingTop: 10
    },
    space: {
        height: 10
    },
    underTaskView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 20,
    },
    archivedImg: {
        height: 17,
        width: 17,
    },
    paddingLeft10: {
        paddingLeft: 10,
    }
});

function Separator() {
    return (
        <ViewInverted style={{ width: '60%', height: 1 }} />
    );
}

function hasTag(id, tag) {
    for (let i in tag) {
        if (tag[i] === id)
            return true;
    }
    return false;
}

function TagAdd(props) {
    return (
        <>
            {props.openTag === props.index ? (
                <View onLayout={props.onLayout} style={{ backgroundColor: props.color, borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                    <TextInput adjustsFontSizeToFit onEndEditing={props.onEndEditing} onChangeText={props.onChangeText} value={props.text} style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, fontFamily: 'avenirbook', marginTop: 5 }} />
                </View>
            ) : (
                    <TouchableOpacity onLongPress={props.onLongPress} onPress={props.onPress} style={{ backgroundColor: props.color, borderRadius: 20, height: 35, alignItems: 'center', justifyContent: 'center', marginHorizontal: 3, marginVertical: 3 }}>
                        <Text adjustsFontSizeToFit style={{ color: 'rgba(255, 255, 255, 1)', paddingHorizontal: 15, textAlign: 'center', fontSize: 15, lineHeight: 18, fontFamily: hasTag(props.id, props.tag) ? 'avenirblack' : 'avenirbook', marginTop: 5 }}>{props.text}</Text>
                    </TouchableOpacity>
                )}
        </>
    );
}

function Tag(props) {
    return (
        <View>
            <Text adjustsFontSizeToFit style={{ letterSpacing: 2, color: props.color, paddingHorizontal: 10, textAlign: 'center', fontSize: 15, lineHeight: 18, fontFamily: 'avenirblack', marginTop: 5 }}>{props.text}</Text>
        </View>
    );
}