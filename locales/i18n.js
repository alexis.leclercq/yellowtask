import memoize from "lodash.memoize";
import i18n from 'i18n-js';

import fr from './fr.json'
import en from './en.json'
import es from './es.json'

i18n.defaultLocale = 'en';
i18n.fallbacks = true;
i18n.translations = { fr, en, es };

export const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

export let locale = i18n.locale;

export async function setLocale(code) {
  i18n.fallbacks = true;
  i18n.translations = { fr, en, es };
  i18n.locale = code;
};
