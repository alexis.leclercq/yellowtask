const tintColorLight = '#FDCD2F';
const tintColorDark = '#148a40';
const tintColorVeryLight = '#8de17c';

export default {
  light: {
    text: 'rgba(0, 0, 0, 0.7)',
    textInverted: '#fff',
    textInput: 'rgba(0, 0, 0, 0.7)',
    textInputLow: 'rgba(0, 0, 0, 0.4)',
    background: 'rgba(255, 255, 255, 0.95)',
    backgroundInverted: '#f5f5f5',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: '#fff',
    textInverted: 'rgba(0, 0, 0, 0.7)',
    textInput: '#fff',
    textInputLow: '#4a4a4a',
    background: '#1f1f1f',
    backgroundInverted: '#4a4a4a',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
  },
};