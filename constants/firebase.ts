import * as firebase from 'firebase';
import { Platform } from 'react-native';

// Optionally import the services that you want to use
import "firebase/auth";
//import "firebase/database";
import "firebase/firestore";
//import "firebase/functions";
import "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
  apiKey: Platform.OS === "android" ? "AIzaSyB_hSHgdEXkvr-I7DSlllVyLagWXguSGfU" : "AIzaSyDVhg13wpEqHG01ZxFHpj7MhQ7SO1ACBqk",
  authDomain: "project-id.firebaseapp.com",
  databaseURL: "https://yellowtask-4d94d.firebaseio.com",
  projectId: "yellowtask-4d94d",
  storageBucket: "yellowtask-4d94d.appspot.com",
  messagingSenderId: "102514521811",
  appId: Platform.OS === "android" ? "1:102514521811:android:e60c855d2b1a02a2348626" : "1:102514521811:ios:91e91173b5bd4f23348626",
  measurementId: "G-measurement-id"
};

firebase.initializeApp(firebaseConfig);

export default firebase;