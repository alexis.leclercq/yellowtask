import React, { Component } from 'react';
import { TouchableOpacity, View, Text, TextInput, Keyboard, Alert } from 'react-native';
import { width } from '../constants/Layout';
import firebase from '../constants/firebase';
import { translate } from '../locales/i18n';

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export default class LoginScreen extends Component<{ navigation: any }, any> {

    state = {
        signup: true,
        email: null,
        password: null,
    };

    async fortgotPassword() {
        if (!validateEmail(this.state.email)) {
            Alert.alert('Merci d\'entrer une adresse mail valide');
            return;
        }
        firebase.auth().sendPasswordResetEmail(this.state.email)
            .then(function (user) {
                Alert.alert('Un mail vous a été envoyé.')
            }).catch(function (e) {
                console.log(e)
            });

    }

    async signInWithLoginEmail() {
        if (!validateEmail(this.state.email.trim())) {
            Alert.alert('Merci d\'entrer une adresse mail valide');
            return;
        }
        if (!this.state.password) {
            Alert.alert('Merci d\'entrer un mot de passe valide');
            return;
        }
        await firebase
            .auth()
            .signInWithEmailAndPassword(this.state.email.trim(), this.state.password)
            .then((e) => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email.trim(), this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        const user = firebase.auth().currentUser;

                        if (!user.uid) {
                            Alert.alert('Merci d\'essayer de vous reconnecter');
                            return;
                        }
                        const id1 = Math.random().toString(36).substring(3);
                        const id2 = Math.random().toString(36).substring(3);
                        const ref = 'local' + user.uid + 'YellowTask' + id1 + id2;
                        await firebase.firestore().collection('users')
                            .doc(user.uid).get().then(async (docRef) => {
                                if (docRef.exists)
                                    await firebase.firestore().collection('users')
                                        .doc(user.uid).update({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                                        .then(() => {
                                            this.props.navigation.navigate('Main', { session: ref })
                                        });
                            })
                    })
                    .catch((error) => {
                        Alert.alert('Une erreur est survenue')
                    });
            }).catch(error => {
                let errorCode = error.code;
                let errorMessage = error.message;
                if (errorCode === 'auth/weak-password') {
                    Alert.alert('Le mot de passe est incorrect')
                } else {
                    Alert.alert(errorMessage)
                }
            });
    }

    async signInWithEmail() {
        console.log("MAIL")
        if (!validateEmail(this.state.email.trim())) {
            Alert.alert('Merci d\'entrer une adresse mail valide');
            return;
        }
        firebase.auth().createUserWithEmailAndPassword(this.state.email.trim(), this.state.password)
            .then((e) => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email.trim(), this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        const user = firebase.auth().currentUser;

                        if (!user.uid) {
                            Alert.alert('Merci d\'essayer de vous reconnecter');
                            return;
                        }
                        const id1 = Math.random().toString(36).substring(3);
                        const id2 = Math.random().toString(36).substring(3);
                        const ref = 'local' + user.uid + 'YellowTask' + id1 + id2;
                        await firebase.firestore().collection('users')
                            .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                            .then(() => {
                                this.props.navigation.state.params.analytics.track("Signed up", { "None": "None" });
                                this.props.navigation.navigate('Main', { session: ref })
                            }).catch(err => console.log(err));
                    })
                    .catch((error) => {
                        console.log(error)
                        Alert.alert('Une erreur est survenue')
                    });
            }).catch((error) => {
                if (String(error).includes('Password')) {
                    Alert.alert('Votre mot de passe doit contenir au moins 6 caractères.');
                    return;
                }
                if (String(error).includes('email')) {
                    Alert.alert('Votre email a déjà été utilisée');
                    return;
                }
                Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊');
            });
    }

    static navigationOptions = {
        headerTitle: translate('logIn'),
        headerTintColor: '#000',
        headerStyle: { backgroundColor: '#fff' },
        headerBackTitleVisible: false,
    };

    emailInput: any;
    passwordInput: any;

    render() {
        return (
            <TouchableOpacity activeOpacity={100} onPress={Keyboard.dismiss} style={{
                backgroundColor: '#fff',
                flex: 1,
            }}>
                <View style={{ padding: 20 }}>
                    <TouchableOpacity onPress={() => this.emailInput.focus()} activeOpacity={1} style={{ paddingVertical: 10, width: '100%', borderBottomWidth: 1, borderBottomColor: 'rgba(0,0,0,0.1)' }}>
                        <TextInput onSubmitEditing={() => this.passwordInput.focus()} ref={(input) => this.emailInput = input} style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4' }} onChangeText={(e) => this.setState({ email: e })} placeholder={translate('eMail')} value={this.state.email} autoCapitalize={"none"} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.passwordInput.focus()} activeOpacity={1} style={{ marginTop: 20, paddingVertical: 10, width: '100%', borderBottomWidth: 1, borderBottomColor: 'rgba(0,0,0,0.1)' }}>
                        <TextInput onSubmitEditing={() => this.signInWithLoginEmail()} secureTextEntry ref={(input) => this.passwordInput = input} style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4' }} onChangeText={(e) => this.setState({ password: e })} placeholder={translate('password')} value={this.state.password} autoCapitalize={"none"} />
                    </TouchableOpacity>

                </View>
                <TouchableOpacity onPress={() => this.signInWithLoginEmail()} style={{ height: 50, width: width - 80, backgroundColor: '#FDCD2F', alignSelf: 'center', borderRadius: 100, alignItems: 'center', marginVertical: 10 }}>
                    <Text style={{ fontFamily: 'gidugu', fontSize: 30, textAlign: 'center', lineHeight: 50 }}>{translate('logIn')}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.fortgotPassword()} style={{ alignSelf: 'center' }}>
                    <Text style={{ textAlign: 'left', fontSize: 18, fontFamily: 'avenirbook', color: '#C4C4C4', marginTop: 20 }}>{translate('forgotPassword')}</Text>
                </TouchableOpacity>
            </TouchableOpacity>
        );
    }
}