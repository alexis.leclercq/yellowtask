import React, { Component, useState, useEffect } from 'react';
import { Image, SafeAreaView, StyleSheet, Text, TextInput, Platform, Alert, ImageBackground, FlatList, AsyncStorage, View, TouchableOpacity } from 'react-native';
import { TextInverted as ThemedTextInverted, TouchableOpacity as ThemedTouchableOpacity, TextInput as ThemedTextInput, Text as ThemedText } from '../components/common/Themed';
import { height, width } from '../constants/Layout';
import firebase from '../constants/firebase';
import CustomCarousel from '../components/CustomCarousel';
import ExpoMixpanelAnalytics from '@benawad/expo-mixpanel-analytics';
import * as SQLite from 'expo-sqlite';
import moment from 'moment';
import { getUserLocalData, getUserOnlineData, increaseTagIndex } from '../api/Users';
import { addTag, editTag, removeTag } from '../api/Tags';
import Confetti from 'react-native-confetti';
import AddCard from '../components/AddCard';
import SliddingMenu from '../components/SliddingMenu';
import { translate } from '../locales/i18n';
import Header from '../components/Header';
import DayMode from '../components/DayMode';

const analytics = new ExpoMixpanelAnalytics('b1baf3361ff286c515c936ea19262112')

analytics.track("Open app", { "Referred By": "Friend" });

const db = SQLite.openDatabase('yellowtask.db');

export default function Main(props: { navigation: any }) {
    const [lists, setLists] = useState([]);
    const [tags, setTags] = useState([]);
    const [addCard, setAddCard] = useState(false);
    const [swipeablePanelActive, setSwipeablePanelActive] = useState(false);
    const [showMore, setShowMore] = useState(false);
    const [showMoreYellowTask, setShowMoreYellowTask] = useState(false);
    const [selected, setSelected] = useState(0);
    const [editing, setEditing] = useState(false);
    const [localData, setLocalData] = useState(null);
    const [hasInternet, setHasInternet] = useState(false);
    const [img, setImg] = useState(null);
    const [newSession, setNewSession] = useState(false);
    const [time, setTime] = useState('day');
    const [infoLoaded, setInfoLoaded] = useState(false);
    const [layout, setLayout] = useState({ height: 0 });
    const [tagIndex, setTagIndex] = useState(null);

    let _confettiView: any;

    const user = firebase.auth().currentUser;

    useEffect(() => {
        if (user && !user.uid) {
            props.navigation.navigate('First');
            return;
        }
        db.transaction(tx => {
            tx.executeSql(
                "create table if not exists users (archive BOOLEAN DEFAULT NULL, last_updated text, lists text, background text, tags text);"
            );
        });
        db.transaction(tx => {
            tx.executeSql("select * from users", [], async (_, { rows }) => {
                if (rows.length >= 1 && rows.item(0).background)
                    setImg(rows.item(0).background);
                else {
                    await firebase.storage().ref().child('backgrounds/' + user.uid).getDownloadURL().then((val) => {
                        if (val)
                            setImg(val);
                    }).catch((err) => null);
                }
                await firebase.firestore().collection('users')
                    .doc(user.uid)
                    .get()
                    .then(async (docRef) => {
                        setHasInternet(true);
                        if (rows.length > 0 && rows.item(0) && rows.item(0).last_updated != undefined && rows.item(0).lists != undefined && rows.item(0).tags != undefined && rows.item(0).tagIndex) {
                            if (docRef.data() && docRef.data().last_updated && moment(docRef.data().last_updated).isBefore(moment(rows.item(0).last_updated))) {
                                console.log("LOCAL DATA NO UPDATE")
                                setLocalData(true);
                                setNewSession(false);
                                let res = await getUserLocalData(db, true, user);
                                setLists(res.lists);
                                setTags(res.tags);
                                setTagIndex(res.tagIndex);
                                setInfoLoaded(true);
                            } else {
                                console.log("LOCAL DATA NEED UPDATE")
                                setLocalData(false);
                                setNewSession(false);
                                let res = await getUserOnlineData(db, user, docRef);
                                setLists(res.lists);
                                setTags(res.tags);
                                setTagIndex(res.tagIndex);
                                setInfoLoaded(true);
                            }
                        } else {
                            if (docRef.exists) {
                                console.log("ONLINE DATA")
                                setLocalData(false);
                                setNewSession(false);
                                let res = await getUserOnlineData(db, user, docRef);
                                setLists(res.lists);
                                setTags(res.tags);
                                setTagIndex(res.tagIndex);
                                setInfoLoaded(true);
                            } else {
                                console.log("ONLINE DATA CREATING ACCOUNT")
                                await firebase.firestore().collection('users')
                                    .doc(user.uid).set({ gender: "undefined", birthday: "undefined", job: "undefined", last_updated: null })
                                    .then(async () => {
                                        setLocalData(false);
                                        setNewSession(true);
                                        let res = await getUserOnlineData(db, user, docRef);
                                        setLists(res.lists);
                                        setTags(res.tags);
                                        setTagIndex(res.tagIndex);
                                        setInfoLoaded(true);
                                    });
                            }
                        }
                    })
                    .catch(async (err) => {
                        console.log(err)
                        setHasInternet(false);
                        setNewSession(false);
                        await AsyncStorage.setItem("yellowtaskDBNeedUpdate", "true");
                        if (rows.length > 0 && rows.item(0) && rows.item(0).last_updated != undefined && rows.item(0).lists != undefined) {
                            console.log("LOCAL DATA")
                            setLocalData(true)
                            let res = await getUserLocalData(db, false, user);
                            setLists(res.lists);
                        } else
                            Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊')
                    });
            }
            );
        });
    }, []);

    const openPanel = () => {
        analytics.track("Open me", { "None": "None" });
        setSwipeablePanelActive(true);
    };

    const addNewTag = async (newTagName, newTagColor) => {
        let res = await addTag(tags, newTagName, newTagColor, tagIndex, db);
        let res2 = await increaseTagIndexGeneral();
        setTagIndex(res2.tagIndex);
        //this.setState({ tags: res.tags });
    }

    const editSelectedTag = async (newTagName, newTagColor, newId, index) => {
        let res = await editTag(tags, newTagName, newTagColor, newId, db, index);
        //this.setState({ tags: res.tags });
    }

    const removeSelectedTag = async (index) => {
        let res = await removeTag(tags, index, db);
        //this.setState({ tags: res.tags });        
    }

    const increaseTagIndexGeneral = async () => {
        let res = await increaseTagIndex(tagIndex, db);
        return res;
        //this.setState({ tags: res.tags });        
    }

    return (
        <ImageBackground source={infoLoaded ? img ? { uri: img } : require('../assets/frameback.png') : require('../assets/splash.png')} style={{ height, width }} resizeMode={'cover'}>
            <Confetti ref={(node) => _confettiView = node} duration={2000} timeout={1} confettiCount={50} colors={["rgb(254, 206, 47)", "rgb(254, 206, 47)", "rgb(254, 206, 47)", "rgb(254, 206, 47)", "rgb(254, 206, 47)"]} />
            <SafeAreaView style={styles.container}>
                <View onLayout={(event) => setLayout(event.nativeEvent.layout)} style={styles.container}>
                    <View style={{ height: layout.height * 0.19355, width: width }}>
                        <View style={{ flexDirection: 'row', flex: 5 }}>
                            <Header db={db} setAddCard={(obj) => setAddCard(obj)} setLists={(obj) => setLists(obj)} setEditing={(obj) => setEditing(obj)} setSelected={(obj) => setSelected(obj)} openPanel={openPanel} editing={editing} analytics={analytics} lists={lists} selected={selected} />
                        </View>
                        <View style={{ flexDirection: 'row', flex: 3, paddingLeft: 30, paddingTop: 15 }}>
                            <DayMode setTime={(obj) => setTime(obj)} analytics={analytics} lists={lists} selected={selected} time={time} />
                        </View>
                    </View>
                    <View style={{ height: layout.height * 0.80645 }}>
                        <CustomCarousel layout={layout.height * 0.80645} time={time} increaseTagIndex={increaseTagIndexGeneral} addTag={(newTagName, newTagColor) => addNewTag(newTagName, newTagColor)} removeTag={(index) => removeSelectedTag(index)} editTag={(newTagName, newTagColor, newId, index) => editSelectedTag(newTagName, newTagColor, newId, index)} updateSession={() => setNewSession(false)} newSession={newSession} confettiRef={_confettiView} selected={selected} lists={lists} tags={tags} analytics={analytics} localData={localData} />
                    </View>
                    <AddCard visible={addCard} setLists={(obj) => setLists(obj)} setSelected={(obj) => setSelected(obj)} setAddCard={(obj) => setAddCard(obj)} hide={() => setAddCard(false)} navigation={props.navigation} analytics={analytics} lists={lists} db={db} />
                    <SliddingMenu swipeablePanelActive={swipeablePanelActive} setImg={(obj) => setImg(obj)} closePanel={() => setSwipeablePanelActive(false)} analytics={analytics} db={db} navigation={props.navigation} />
                </View>
            </SafeAreaView>
        </ImageBackground>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    shadow: Platform.OS === "ios" && {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 7,
    }
});