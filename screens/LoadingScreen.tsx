import React, { Component } from 'react';
import { Image } from 'react-native';
import { height, width } from '../constants/Layout';
import firebase from '../constants/firebase';
import * as Localization from 'expo-localization';
import { setLocale } from '../locales/i18n';

export default class LoadingScreen extends Component<{ navigation: any }, any> {
    async componentDidMount() {
        await setLocale(Localization.locale);
        firebase.auth().onAuthStateChanged(async (user) => {
            if (user != null) {
                const id1 = Math.random().toString(36).substring(3);
                const id2 = Math.random().toString(36).substring(3);
                const ref = 'local' + user.uid + 'YellowTask' + id1 + id2;
                this.props.navigation.navigate('Main', { session: ref });
            } else {
                this.props.navigation.navigate('First');
            }
        });
    }

    signOut = async () => {
        firebase.auth().signOut().then(() => {
            this.setState({ showMore: false, swipeablePanelActive: false });
            this.props.navigation.navigate('First')
        }, function (error) {
            // An error happened.
        });
    };

    render() {
        return (
            <Image style={{ height, width }} source={require('../assets/splash.png')} />
        );
    }
}