import React, { Component } from 'react';
import { StyleSheet, View, ImageBackground, TouchableOpacity, Text } from 'react-native';
import { height, width } from '../constants/Layout';
import * as SQLite from 'expo-sqlite';
import ExpoMixpanelAnalytics from '@benawad/expo-mixpanel-analytics';
import { translate } from '../locales/i18n';

const analytics = new ExpoMixpanelAnalytics('b1baf3361ff286c515c936ea19262112')

const db = SQLite.openDatabase('yellowtask.db');

export default class FirstScreen extends Component<{ navigation: any }, any> {

    componentDidMount() {
        db.transaction(
            tx => {
                tx.executeSql("drop table if exists users");
                tx.executeSql("drop table if exists lists");
                tx.executeSql("drop table if exists pages");
                tx.executeSql("drop table if exists bubbles");
            });
    }

    render() {
        return (
            <View style={styles.slide}>
                <ImageBackground source={require('../assets/Login.png')} style={{ height: '100%', width: '100%', alignItems: 'center' }}>
                    <View style={{ flex: 1 }} />
                    <View style={{ flex: 9 }}>
                        <Text style={{ fontFamily: 'gidugu', fontSize: 50, textAlign: 'center', lineHeight: 70, marginTop: -10 }}>{translate('weHelpTo1')}</Text>
                        <Text style={{ fontFamily: 'gidugu', fontSize: 50, textAlign: 'center', lineHeight: 70, marginTop: -40 }}>{translate('weHelpTo2')}</Text>
                    </View>
                    <View style={{ flex: 3, marginHorizontal: 40 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Register', { analytics })} style={{ height: 50, width: width - 80, backgroundColor: '#FDCD2F', borderRadius: 100, alignItems: 'center', marginVertical: 10 }}>
                            <Text style={{ fontFamily: 'gidugu', fontSize: 30, textAlign: 'center', lineHeight: 50 }}>{translate('register')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Login', { analytics })} style={{ height: 50, width: width - 80, borderColor: '#FDCD2F', borderRadius: 100, borderWidth: 5, alignItems: 'center', marginVertical: 10 }}>
                            <Text style={{ fontFamily: 'gidugu', fontSize: 30, textAlign: 'center', lineHeight: 50, color: '#FDCD2F' }}>{translate('iHaveAccount')}</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bubble_text: {
        marginTop: 5,
        marginLeft: 15,
        color: '#000',
        fontFamily: 'avenirbook',
        fontSize: 17,
        flexGrow: 1,
    },
    create_with: {
        flex: 2,
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    create_with_sec: {
        flex: 3,
        width: width,
        alignItems: 'center',
    },
    bubble_logo: {
        height: 50,
        width: width * 0.8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    go_design: {
        height: 15,
        width: 15,
        marginRight: 20,
    },
    create_account_design: {
        height: '100%',
        width: '60%'
    },
    create_design: {
        height: '100%',
        width: '90%'
    },
    create_bubble: {
        height: 50,
        marginVertical: 10,
        width: width * 0.8,
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    wrapper: {
        height: height * 0.6,
    },
    slide: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },
    underlined: {
        textDecorationLine: "underline"
    },
    subpart: {
        justifyContent: 'center',
        alignItems: 'center',
        height: height * /*0.17*/ 0.22,
        paddingHorizontal: 20,
    },
    textlittle: {
        textAlign: 'center',
        fontSize: 11,
        color: '#C4C4C4',
        fontFamily: 'avenirbook'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0, },
        shadowOpacity: 0.20,
        shadowRadius: 15,
        elevation: 15
    }
});