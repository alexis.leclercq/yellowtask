import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import { MainParamList } from '../types';
import LoadingScreen from '../screens/LoadingScreen';
import FirstScreen from '../screens/FirstScreen';
import RegisterScreen from '../screens/RegisterScreen';
import LoginScreen from '../screens/LoginScreen';
import MainScreen from '../screens/MainScreen';
import Colors from '../constants/Colors';
import { translate } from '../locales/i18n';

const MainTab = createStackNavigator<MainParamList>();

export default function LoadingNavigator() {
  return (
    <MainTab.Navigator
      initialRouteName="Loading">
      <MainTab.Screen
        name="Loading"
        component={LoadingScreen}
        options={{ headerShown: false, animationEnabled: false }}
      />
      <MainTab.Screen
        name="First"
        component={FirstScreen}
        options={{ headerShown: false, gestureEnabled: false, animationEnabled: false }}
      />
      <MainTab.Screen
        name="Register"
        component={RegisterScreen}
        options={{ headerBackTitleVisible: false, headerTintColor: Colors.light.tint, headerTitle: translate('register') }}
      />
      <MainTab.Screen
        name="Login"
        component={LoginScreen}
        options={{ headerBackTitleVisible: false, headerTintColor: Colors.light.tint, headerTitle: translate('logIn') }}
      />
      <MainTab.Screen
        name="Main"
        component={MainScreen}
        options={{ headerShown: false, gestureEnabled: false }}
      />
    </MainTab.Navigator>
  );
}