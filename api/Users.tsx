import moment from 'moment';
import firebase from '../constants/firebase';
import { AsyncStorage, Alert } from 'react-native';
import { translate } from '../locales/i18n';

export async function getUserOnlineData(db, user, docRef) {
    await firebase.firestore().collection("users").doc(user.uid).update({ last_updated: moment().toISOString() }).catch((error) => {
        console.log(error);
    });
    let res = { archive: null, lists: null, tags: null, tagIndex: "-1" };
    db.transaction(
        tx => {
            tx.executeSql("drop table if exists users");
            tx.executeSql("drop table if exists lists");
            tx.executeSql(
                "create table if not exists users (archive BOOLEAN DEFAULT NULL, last_updated text, lists text, background text, tags text, tagIndex text);"
            );
            tx.executeSql(
                "create table if not exists lists (id text, name text, type text, pages text, bubbles text);"
            );
            tx.executeSql(
                "create table if not exists bubbles (id text, archived text, date text, days text, name text, reminders text, repeat text, note text, tag text, underTask text, open text);"
            );
            tx.executeSql(
                "create table if not exists pages (id text, name text, bubbles text);"
            );
        },
        null,
    );
    db.transaction(
        tx => {
            tx.executeSql("insert into users (last_updated, lists, tags, tagIndex) values (?, ?, ?, ?)", [new Date().toUTCString(), JSON.stringify([]), JSON.stringify([]), "5"]);
        },
        null,
    );
    if (docRef.exists) {
        if (docRef.data().archive) {
            db.transaction(
                tx => {
                    tx.executeSql("update users set archive = (?)", [docRef.data().archive]);
                },
                null,
            );
            res = { ...res, archive: docRef.data().archive };
        }
        if (docRef.data().tagIndex) {
            db.transaction(
                tx => {
                    tx.executeSql("update users set tagIndex = (?)", [docRef.data().tagIndex]);
                },
                null,
            );
            res = { ...res, tagIndex: docRef.data().tagIndex };
        } else {
            db.transaction(
                tx => {
                    tx.executeSql("update users set tagIndex = (?)", ["5"]);
                },
                null,
            );
            res = { ...res, tagIndex: "5" };
        }
        if (docRef.data().tags) {
            db.transaction(
                tx => {
                    tx.executeSql("update users set tags = (?)", [JSON.stringify(docRef.data().tags)]);
                },
                null,
            );
            res = { ...res, tags: docRef.data().tags };
        } else {
            db.transaction(
                tx => {
                    tx.executeSql("update users set tags = (?)", [JSON.stringify([{ name: translate('job'), color: '#FECE2F', id: 0 }, { name: translate('appointment'), color: '#FF7070', id: 1 }, { name: translate('sport'), color: '#16B476', id: 2 }, { name: translate('purchase'), color: '#8235FF', id: 3 }, { name: translate('other'), color: '#BABABA', id: 4 }])]);
                },
                null,
            );
            await AsyncStorage.setItem("yellowtaskDBNeedUpdate", "true");
            res = { ...res, tags: [{ name: translate('job'), color: '#FECE2F', id: 0 }, { name: translate('appointment'), color: '#FF7070', id: 1 }, { name: translate('sport'), color: '#16B476', id: 2 }, { name: translate('purchase'), color: '#8235FF', id: 3 }, { name: translate('other'), color: '#BABABA', id: 4 }] };
        }
        if (docRef.data().lists) {
            let lists = [];
            let localLists = [];
            for (let i in docRef.data().lists) {
                await firebase.firestore().collection("lists").doc(docRef.data().lists[i])
                    .get()
                    .then((docRef2) => {
                        lists.push({ id: i, name: docRef2.data().name, type: docRef2.data().type, ref: docRef2.id });
                        if (!!docRef2.data().bubbles) {
                            db.transaction(
                                tx => {
                                    tx.executeSql("insert into lists (id, name, type, bubbles) values (?, ?, ?, ?)", [docRef2.id, docRef2.data().name, docRef2.data().type, docRef2.data().bubbles ? JSON.stringify(docRef2.data().bubbles) : null]);
                                },
                                null,
                            );
                        } else if (!!docRef2.data().pages) {
                            db.transaction(
                                tx => {
                                    tx.executeSql("insert into lists (id, name, type, pages) values (?, ?, ?, ?)", [docRef2.id, docRef2.data().name, docRef2.data().type, docRef2.data().pages ? JSON.stringify(docRef2.data().pages) : null]);
                                },
                                null,
                            );
                        }
                        localLists.push(docRef2.id);
                    })
                    .catch((error) => Alert.alert('Oups, il semble que vous n\'êtes pas connecté à Internet. Veuillez réessayer 😊'));
            }
            db.transaction(
                tx => {
                    tx.executeSql("update users set lists = (?)", [JSON.stringify(localLists)]);
                },
                null,
            );
            res = { ...res, lists };
        } else {
            const id1 = Math.random().toString(36).substring(3);
            const id2 = Math.random().toString(36).substring(3);
            const bubbleRef = 'local' + user.uid + 'YellowTask' + id1 + id2;
            const id12 = Math.random().toString(36).substring(3);
            const id22 = Math.random().toString(36).substring(3);
            const bubbleRef2 = 'local' + user.uid + 'YellowTask' + id12 + id22;
            const id1L = Math.random().toString(36).substring(3);
            const id2L = Math.random().toString(36).substring(3);
            const listRef = 'local' + user.uid + 'YellowTask' + id1L + id2L;
            let lists = [];
            db.transaction(
                tx => {
                    tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                        bubbleRef,
                        translate('firstBubbleTitle'),
                        translate('firstBubbleNote'),
                        JSON.stringify([4]),
                        JSON.stringify([
                            { id: 0, name: translate('firstBubbleFirstSubTitle'), archived: false },
                            { id: 1, name: translate('firstBubbleSecondSubTitle'), archived: false },
                        ]),
                        "false"
                    ]);
                    tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                        bubbleRef2,
                        translate('secondBubbleTitle'),
                        "",
                        JSON.stringify([]),
                        JSON.stringify([]),
                        "false"
                    ]);
                    tx.executeSql("insert into lists (id, name, type, bubbles) values (?, ?, ?, ?)", [listRef, translate('welcome'), "calendar", JSON.stringify([bubbleRef, bubbleRef2])]);
                    tx.executeSql("update users set lists = (?)", [JSON.stringify([listRef])]);
                }
            );
            await firebase.firestore().collection('bubbles').doc(bubbleRef).set({
                name: translate('firstBubbleTitle'),
                note: translate('firstBubbleNote'),
                tag: [4],
                underTask: [
                    { id: 0, name: translate('firstBubbleFirstSubTitle'), archived: false },
                    { id: 1, name: translate('firstBubbleSecondSubTitle'), archived: false },
                ],
                open: "false",
            });
            await firebase.firestore().collection('bubbles').doc(bubbleRef2).set({
                name: translate('secondBubbleTitle'),
                note: "",
                tag: [],
                underTask: [],
                open: "false",
            });
            await firebase.firestore().collection('lists').doc(listRef).set({ name: translate('welcome'), type: 'calendar', bubbles: [bubbleRef, bubbleRef2] });
            await firebase.firestore().collection('users').doc(user.uid).update({ lists: [listRef], tags: [{ name: translate('job'), color: '#FECE2F' }, { name: translate('appointment'), color: '#FF7070' }, { name: translate('sport'), color: '#16B476' }, { name: translate('purchase'), color: '#8235FF' }, { name: translate('other'), color: '#BABABA' }] }).catch(error => console.log(error));
            lists.push({ id: 0, name: translate('welcome'), type: "calendar", ref: listRef });
            res = { ...res, lists };
        }
    } else {
        const id1 = Math.random().toString(36).substring(3);
        const id2 = Math.random().toString(36).substring(3);
        const bubbleRef = 'local' + user.uid + 'YellowTask' + id1 + id2;
        const id12 = Math.random().toString(36).substring(3);
        const id22 = Math.random().toString(36).substring(3);
        const bubbleRef2 = 'local' + user.uid + 'YellowTask' + id12 + id22;
        const id1L = Math.random().toString(36).substring(3);
        const id2L = Math.random().toString(36).substring(3);
        const listRef = 'local' + user.uid + 'YellowTask' + id1L + id2L;
        let lists = [];
        db.transaction(
            tx => {
                tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                    bubbleRef,
                    translate('firstBubbleTitle'),
                    translate('firstBubbleNote'),
                    JSON.stringify([4]),
                    JSON.stringify([
                        { id: 0, name: translate('firstBubbleFirstSubTitle'), archived: false },
                        { id: 1, name: translate('firstBubbleSecondSubTitle'), archived: false },
                    ]),
                    "false"
                ]);
                tx.executeSql("insert into bubbles (id, name, note, tag, underTask, open) values (?, ?, ?, ?, ?, ?)", [
                    bubbleRef2,
                    translate('secondBubbleTitle'),
                    "",
                    JSON.stringify([]),
                    JSON.stringify([]),
                    "false"
                ]);
                tx.executeSql("insert into lists (id, name, type, bubbles) values (?, ?, ?, ?)", [listRef, translate('welcome'), "calendar", JSON.stringify([bubbleRef, bubbleRef2])]);
                tx.executeSql("update users set lists = (?)", [JSON.stringify([listRef])]);
            }
        );
        await firebase.firestore().collection('bubbles').doc(bubbleRef).set({
            name: translate('firstBubbleTitle'),
            note: translate('firstBubbleNote'),
            tag: [4],
            underTask: [
                { id: 0, name: translate('firstBubbleFirstSubTitle'), archived: false },
                { id: 1, name: translate('firstBubbleSecondSubTitle'), archived: false },
            ],
            open: "false",
        });
        await firebase.firestore().collection('bubbles').doc(bubbleRef2).set({
            name: translate('secondBubbleTitle'),
            note: "",
            tag: [],
            underTask: [],
            open: "false",
        });
        await firebase.firestore().collection('lists').doc(listRef).set({ name: translate('welcome'), type: 'calendar', bubbles: [bubbleRef, bubbleRef2] });
        await firebase.firestore().collection('users').doc(user.uid).update({ lists: [listRef], tagIndex: "5", tags: [{ name: translate('job'), color: '#FECE2F' }, { name: translate('appointment'), color: '#FF7070' }, { name: translate('sport'), color: '#16B476' }, { name: translate('purchase'), color: '#8235FF' }, { name: translate('other'), color: '#BABABA' }] }).catch((err) => console.log(err));
        lists.push({ id: 0, name: translate('welcome'), type: "calendar", ref: listRef });
        res = { ...res, lists, tagIndex: "5" };
    }
    return res;
};

export async function getUserLocalData(db, hasInternet, user) {
    let res: { archive?: any, lists?: any, tags?: any, tagIndex?: string } = { archive: null, lists: null, tags: null, tagIndex: "-1" };
    await new Promise(async (resolve, reject) => {
        db.transaction(
            tx => {
                tx.executeSql("update users set last_updated = (?)", [moment().toISOString()]);
                tx.executeSql("select * from users", [], async (_, { rows }) => {
                    if (rows.length > 0) {
                        let archiveData = rows.item(0).archive === 0;
                        let localTags = JSON.parse(rows.item(0).tags);
                        let localLists = JSON.parse(rows.item(0).lists);
                        let tagIndex = rows.item(0).tagIndex;
                        let command = "";
                        command += "id = '" + localLists[0] + "'";
                        for (let i = 1; localLists[i]; i++)
                            if (localLists[i] !== "")
                                command += " or id = '" + localLists[i] + "'";
                        let lists = [];
                        tx.executeSql("select * from lists where " + command, [], async (_, { rows }) => {
                            for (let i = 0; i !== rows.length; i++) {
                                let p = rows.item(i);
                                lists.push({ id: i, name: p.name, type: p.type/*, bubbles: p.bubbles, pages: p.pages*/, ref: p.id });
                            }
                            if (await AsyncStorage.getItem('yellowtaskDBNeedUpdate') && hasInternet) {
                                await AsyncStorage.removeItem('yellowtaskDBNeedUpdate');
                                console.log("Need upload local data")
                                let onlineLists = [];
                                for (let i in lists)
                                    onlineLists.push(lists[i].ref)
                                firebase.firestore().collection("/users").doc(user.uid).update({ archive: archiveData, lists: onlineLists, tags: localTags, tagIndex }).catch((error) => console.log(error));
                            }
                            resolve({ archive: archiveData, lists, tags: localTags, tagIndex });
                        });
                    }
                });
            },
            null,
        );
    }).then((value) => {
        res = value;
    });
    return res;
}

export async function increaseTagIndex(tagIndex, db) {
    let res: { tagIndex?: string } = { tagIndex: "-1" };

    await new Promise(async (resolve, reject) => {
        let newTagIndex = parseInt(tagIndex) + 1;
        db.transaction(
            tx => {
                tx.executeSql("update users set tagIndex = (?)", [newTagIndex.toString()]);
            },
            null,
        );
        await AsyncStorage.setItem("yellowtaskDBNeedUpdate", "true");
        resolve({ tagIndex: newTagIndex.toString() });
    }).then((value) => {
        res = value;
    });
    return res;
}
