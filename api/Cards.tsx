import { Alert, AsyncStorage } from 'react-native';

export async function addCard(state, user, db, lists) {
    let res = { lists: null, selected: null, cardName: null, cardType: null, addCard: null };

        const id1 = Math.random().toString(36).substring(3);
        const id2 = Math.random().toString(36).substring(3);
        const ref = 'local' + user.uid + 'YellowTask' + id1 + id2;
        if (state.cardType === 'calendar') {
            db.transaction(
                tx => {
                    tx.executeSql("select * from lists where id = (?)", [ref], async (_, { rows }) => {
                        if (rows.length == 0) {
                            tx.executeSql("insert into lists (id, name, type, bubbles) values (?, ?, ?, ?)", [
                                ref,
                                state.cardName,
                                state.cardType,
                                JSON.stringify([]),
                            ]);
                            lists.push({
                                id: lists.length,
                                name: state.cardName,
                                type: state.cardType,
                                ref: ref,
                                bubbles: [],
                            });
                            let localLists = [];
                            for (let i in lists)
                                localLists.push(lists[i].ref);
                            tx.executeSql("update users set lists = (?)", [JSON.stringify(localLists)]);
                            res = {
                                lists: lists,
                                selected: lists.length - 1,
                                cardName: undefined,
                                cardType: 'calendar',
                                addCard: false,
                            };
                            AsyncStorage.setItem("yellowtaskDBNeedUpdate" + ref, "true");
                        } else {
                            res = { lists: lists, selected: state.selected, cardName: undefined, cardType: 'calendar', addCard: false };
                        }
                    });
                }
            );
        } else {
            db.transaction(
                tx => {
                    tx.executeSql("select * from lists where id = (?)", [ref], async (_, { rows }) => {
                        if (rows.length == 0) {
                            tx.executeSql("insert into lists (id, name, type, pages) values (?, ?, ?, ?)", [
                                ref,
                                state.cardName,
                                state.cardType,
                                JSON.stringify([]),
                            ]);
                            lists.push({
                                id: lists.length,
                                name: state.cardName,
                                type: state.cardType,
                                ref: ref,
                                pages: [],
                            });
                            let localLists = [];
                            for (let i in lists)
                                localLists.push(lists[i].ref);
                            tx.executeSql("update users set lists = (?)", [JSON.stringify(localLists)]);
                            res = {
                                lists: lists,
                                selected: lists.length - 1,
                                cardName: undefined,
                                cardType: 'calendar',
                                addCard: false,
                            };
                            AsyncStorage.setItem("yellowtaskDBNeedUpdate" + ref, "true");
                        } else {
                            res = { lists: lists, selected: state.selected, cardName: undefined, cardType: 'calendar', addCard: false };
                        }
                    });
                }
            );
        }
    return res;
}

export function editCard(state, id, db) {
    let res = { selected: null, editing: null };

    if (state.lists[id].name === "") {
        Alert.alert("Merci de ne pas laisser un nom de carte vide");
        return;
    }
    AsyncStorage.setItem("yellowtaskDBNeedUpdate" + state.lists[id].ref, "true");
    db.transaction(
        tx => {
            tx.executeSql("update lists set name = (?) where id = (?)", [state.lists[id].name, state.lists[id].ref]);
        },
        null,
    );
    res = { ...res, selected: id, editing: false }
    return res;
}

export function removeCard(lists, selected, db) {
    let res = { lists: null, selected: null, editing: null };

    let newList = [];
    let newListRef = [];
    let newId = 0;
    for (let i in lists) {
        if (parseInt(i) !== parseInt(selected)) {
            newList.push({ ...lists[i], id: newId });
            newListRef.push(lists[i].ref);
            newId++;
        }
    }
    res = {
        ...res,
        lists: newList,
    };
    AsyncStorage.setItem("yellowtaskDBNeedUpdate", "true");
    db.transaction(
        tx => {
            tx.executeSql("update users set lists = (?)", [JSON.stringify(newListRef)]);
        },
        null,
    );
    res = { ...res, selected: (selected - 1) <= 0 ? -1 : (selected - 1), editing: false };
    return res;
}