import { AsyncStorage } from 'react-native';

export async function addTag(persistentTags, newTagName, newTagColor, tagIndex, db) {
    let res = {};

    await new Promise(async (resolve, reject) => {
        if (newTagName) {
            let newId = parseInt(tagIndex);
            persistentTags.push({ name: newTagName, color: newTagColor, id: newId });
            console.log(persistentTags)
            db.transaction(
                tx => {
                    tx.executeSql("update users set tags = (?)", [JSON.stringify(persistentTags)]);
                },
                null,
            );
            await AsyncStorage.setItem("yellowtaskDBNeedUpdate", "true");
        }
        resolve({ tags: persistentTags });
    }).then((value) => {
        res = value;
    });
    return res;
}

export async function editTag(persistentTags, newTagName, newTagColor, newId, db, index) {
    let res = {};

    await new Promise(async (resolve, reject) => {
        if (newTagName) {
            persistentTags[index] = { name: newTagName, color: newTagColor, id: newId };
            db.transaction(
                tx => {
                    tx.executeSql("update users set tags = (?)", [JSON.stringify(persistentTags)]);
                },
                null,
            );
            await AsyncStorage.setItem("yellowtaskDBNeedUpdate", "true");
        }
        resolve({ tags: persistentTags });
    }).then((value) => {
        res = value;
    });
    return res;
}

export async function removeTag(persistentTags, index, db) {
    let res = {};

    await new Promise(async (resolve, reject) => {
        if (index >= 0) {
            persistentTags.splice(index, 1);
            db.transaction(
                tx => {
                    tx.executeSql("update users set tags = (?)", [JSON.stringify(persistentTags)]);
                },
                null,
            );
            await AsyncStorage.setItem("yellowtaskDBNeedUpdate", "true");
        }
        resolve({ tags: persistentTags });
    }).then((value) => {
        res = value;
    });
    return res;
}