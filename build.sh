#
# BUILDING EXPO PROJECT SCRIPT
# author: Alexis Leclercq - contact@alexis-leclercq.fr
#

L_BLUE='\033[1;34m'
GREEN='\033[0;32m'
NC='\033[0m'

echo "${L_BLUE}Optimizing assets..."
npx optimize
echo "${L_BLUE}Building iOS Bundle..."
expo build:ios
echo "${GREEN}iOS build successfully created"
echo "${L_BLUE}Building Android Bundle..."
expo build:android -t app-bundle
echo "${GREEN}Android build successfully created"
