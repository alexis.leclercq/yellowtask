export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type NotFoundParamList = {
  NotFoundScreen: undefined;
};

export type MainParamList = {
  Loading: undefined;
  First: undefined;
  Register: undefined;
  Login: undefined;
  Main: undefined;
};

export type SettingsParamList = {
  SettingsScreen: undefined;
};